@echo off
GoliathData -generateAll -in="ViewEdit.razt" -out="Edit.cshtml" -namespace="Goliath.Cms.Data"^
 -templateFolder="%~dp0Templates"^
 -workingFolder="%~dp0Temp"^
 -m="%~dp0GoliathCmsHosting\data.map.config"
 
GoliathData -generateAll -in="ViewList.razt" -out="List.cshtml" -namespace="Goliath.Cms.Data"^
 -templateFolder="%~dp0Templates"^
 -workingFolder="%~dp0Temp"^
 -m="%~dp0GoliathCmsHosting\data.map.config"
pause
