//--------------------------------------------------------------------------------------------------
//<auto-generated>
//
//	This code was generated by a tool.
//	GoliathData Version: 1.3.2.2
//
//	Changes to this file may cause incorrect behavior and will be lost if
//	the code is regenerated. Please use a partial class to add functionalities.
//
//	Copyright Emmanuel Hameyie 2011-2016
//
//</auto-generated>
//--------------------------------------------------------------------------------------------------
// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using Goliath.Cms.Data;

namespace Goliath.Cms.Dtos
{
    [Serializable]
    public partial class UserRolePermDto : IEquatable<UserRolePermDto>
    {
        public UserRolePermDto()
        {
        }

        public UserRolePermDto(UserRolePerm entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            Id = entity.Id;
            ResourceId = entity.ResourceId;
            RoleNumber = entity.RoleNumber;
            PermValue = entity.PermValue;
            CreatedOn = entity.CreatedOn;
            CreatedBy = entity.CreatedBy;
            ModifiedOn = entity.ModifiedOn;
            ModifiedBy = entity.ModifiedBy;
        }

        #region Primary Key

        public long Id { get; set; }

        #endregion

        public static implicit operator UserRolePermDto(UserRolePerm entity)
        {
            return entity == null ? null : new UserRolePermDto(entity);
        }

        public static implicit operator UserRolePerm(UserRolePermDto dto)
        {
            if (dto == null) return null;
            var entity = new UserRolePerm
            {
                Id = dto.Id,
                ResourceId = dto.ResourceId,
                RoleNumber = dto.RoleNumber,
                PermValue = dto.PermValue,
                CreatedOn = dto.CreatedOn,
                CreatedBy = dto.CreatedBy,
                ModifiedOn = dto.ModifiedOn,
                ModifiedBy = dto.ModifiedBy
            };
            return entity;
        }

        #region properties

        public int ResourceId { get; set; }
        public int RoleNumber { get; set; }
        public int PermValue { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }

        #endregion

        #region Equatable

        /// <summary>
        ///     Determines whether the specified <see cref="UserRolePermDto" /> is equal to this instance.
        /// </summary>
        /// <param name="other">The <see cref="UserRolePermDto" /> to compare with this instance.</param>
        /// <returns>
        ///     <c>true</c> if the specified <see cref="UserRolePermDto" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public bool Equals(UserRolePermDto other)
        {
            if (other == null)
                return false;

            return Equals(other.Id, Id);
        }

        /// <summary>
        ///     Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        ///     A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.
        /// </returns>
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        ///     Determines whether the specified <see cref="System.Object" /> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///     <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (obj is UserRolePerm)
                return Equals((UserRolePermDto) obj);

            return base.Equals(obj);
        }

        #endregion
    }
}