﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goliath.Cms.Data;

namespace Goliath.Cms.Models
{
    public class SecurityDashboardModel
    {
        public long TotalUserCount { get; set; }
        public int PageCount { get; set; }

        public ICollection<User> Users { get; set; } = new List<User>();

        public ICollection<UserAccessLog> AccessLog { get; set; } = new List<UserAccessLog>();
    }

    public class AddUserToRoleModel
    {
        public long RoleId { get; set; }
        public long[] SelectedIds { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class LdapGroupModel
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the display name.
        /// </summary>
        /// <value>
        /// The display name.
        /// </value>
        public string DisplayName { get; set; }
        /// <summary>
        /// Gets or sets the name of the distinguished.
        /// </summary>
        /// <value>
        /// The name of the distinguished.
        /// </value>
        public string DistinguishedName { get; set; }
    }
}
