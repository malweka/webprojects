﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Goliath.Cms.Data
{
    public abstract class BaseEntity
    {
        public long Id { get; set; }

        /// <summary>
        /// Notifies the change.
        /// </summary>
        /// <param name="propName">Name of the property.</param>
        /// <param name="value">The value.</param>
        protected abstract void NotifyChange(string propName, object value);
    }
}
