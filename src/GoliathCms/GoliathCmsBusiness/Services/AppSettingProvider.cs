﻿// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goliath.Cms.Data;
using Goliath.Data;
using Goliath.Models;

namespace Goliath.Cms.Services
{
    public class AppSettingProvider : InMemoryCachedSettingsProvider
    {
        private IDatabaseProvider _dbProvider;

        public AppSettingProvider(IDatabaseProvider dbProvider)
        {
            _dbProvider = dbProvider;
        }

        protected override void LoadAll(IDictionary<string, string> cache)
        {
            if (isLoaded) return;

            using (var session = _dbProvider.SessionFactory.OpenSession())
            {
                var dataAdapter = session.GetEntityDataAdapter<AppSetting>();
                var settings = dataAdapter.FetchAll();

                foreach (var setting in settings.Where(setting => !cache.ContainsKey(setting.KeyName)))
                {
                    cache.Add(setting.KeyName, setting.KeyValue);
                }
            }

            isLoaded = true;
        }

        protected override void UpdateSetting(string key, string value, VisualEditorType editorType)
        {
            using (var session = _dbProvider.SessionFactory.OpenSession())
            {
                var adapter = session.GetEntityDataAdapter<AppSetting>();
                var setting = adapter.GetByKeyName(key);

                if (setting == null)
                    throw new GoliathDataException($"Could not update settings. Setting {key} not found.");

                setting.KeyValue = value;
                setting.VisualEditor = editorType;
                setting.UpdatedOn = DateTime.UtcNow;

                adapter.Update(setting);
            }
        }

        protected override void AddSetting(string key, string value, VisualEditorType editorType)
        {
            using (var session = _dbProvider.SessionFactory.OpenSession())
            {
                var adapter = session.GetEntityDataAdapter<AppSetting>();
                var setting = new AppSetting { KeyValue = value, CreatedOn = DateTime.UtcNow };
                adapter.Insert(setting);
            }
        }
    }
}
