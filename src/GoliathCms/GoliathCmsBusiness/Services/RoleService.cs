﻿using System;
using System.Collections.Generic;
using Goliath.Cms.Data;
using Goliath.Data;
using Goliath.Models;
using Goliath.Web.Authorization;

namespace Goliath.Cms.Services
{
    public partial class RoleService
    {
        public ICollection<UserSession> GetAllUsersInRole(long roleId, int limit, int offset, out long total)
        {
            if (!CurrentContext.AuthorizatonValidator.CanView(typeof(Role)))
                throw new SecurityException($"No sufficient permissions to perform *Read* action on entitity of type {typeof(Role)}.");
            try
            {
                if (!CurrentContext.AuthorizatonValidator.CanView(typeof(PropertyNames.Role)))
                    throw new SecurityException($"No sufficient permissions to perform Read on {typeof(Role)}");

                using (var session = CurrentContext.SessionFactory.OpenSession())
                {
                    total = session.GetUserInRoleCount(roleId);
                    return GetAllUsersInRoleInternal(session, roleId, limit, offset);
                }
            }
            catch (Exception ex)
            {
                throw new GoliathException($"Error while trying to read entity Role", ex);
            }
        }

        static ICollection<UserSession> GetAllUsersInRoleInternal(ISession session, long roleId, int limit, int offset)
        {
            return session.GetAllUsersInRole(roleId, limit, offset);
        }

        public ICollection<UserSession> GetAllUsersInRole(long roleId, int limit, int offset)
        {
            if (!CurrentContext.AuthorizatonValidator.CanView(typeof(Role)))
                throw new SecurityException($"No sufficient permissions to perform *Read* action on entitity of type {typeof(Role)}.");
            try
            {
                if (!CurrentContext.AuthorizatonValidator.CanView(typeof(Role)))
                    throw new SecurityException($"No sufficient permissions to perform Read on {typeof(Role)}");

                using (var session = CurrentContext.SessionFactory.OpenSession())
                {
                    return GetAllUsersInRoleInternal(session, roleId, limit, offset);
                }
            }
            catch (Exception ex)
            {
                throw new GoliathException($"Error while trying to read entity Role", ex);
            }
        }

        public IDictionary<int, PermissionActionModel> GetAllPermissions(int roleNumber)
        {
            if (!CurrentContext.AuthorizatonValidator.CanView(typeof(Role)))
                throw new SecurityException($"No sufficient permissions to perform *Read* action on entitity of type {typeof(Role)}.");

            try
            {
                using (var session = CurrentContext.SessionFactory.OpenSession())
                {
                    var perms = session.SelectAll<UserRolePerm>().Where(c => c.RoleNumber).EqualToValue(roleNumber).FetchAll();
                    var permissionList = new Dictionary<int, PermissionActionModel>();
                    foreach (var userRolePerm in perms)
                    {
                        if (permissionList.ContainsKey(userRolePerm.ResourceId)) continue;
                        permissionList.Add(userRolePerm.ResourceId, new PermissionActionModel()
                        {
                            ResourceId = userRolePerm.ResourceId,
                            RoleNumber = userRolePerm.RoleNumber,
                            PermValue = userRolePerm.PermValue
                        });
                    }

                    return permissionList;
                }
            }
            catch (Exception ex)
            {
                throw new GoliathException($"Error while trying to read entity Role", ex);
            }
        }

        public void AssociateUserToRole(long roleId, params long[] userIds)
        {
            if (!CurrentContext.AuthorizatonValidator.CanEdit(typeof(Role)))
                throw new SecurityException($"No sufficient permissions to perform *Read* action on entitity of type {typeof(Role)}.");

            if (userIds == null || userIds.Length == 0)
                return;

            using (var session = CurrentContext.SessionFactory.OpenSession())
            {
                var roleAdapter = session.GetEntityDataAdapter<Role>();
                session.BeginTransaction();

                try
                {
                    foreach (var userId in userIds)
                    {
                        roleAdapter.AssociateRoleWithUser(new User() { Id = userId }, new Role() { Id = roleId });
                    }

                    session.CommitTransaction();
                }
                catch (Exception exception)
                {
                    session.RollbackTransaction();
                    throw new GoliathException($"Error while associating user to role: {roleId}", exception);
                }
            }
        }

        public void DisassociateUser(long roleId, params long[] userIds)
        {
            if (!CurrentContext.AuthorizatonValidator.CanEdit(typeof(Role)))
                throw new SecurityException($"No sufficient permissions to perform *Read* action on entitity of type {typeof(Role)}.");

            if (userIds == null || userIds.Length == 0)
                return;

            using (var session = CurrentContext.SessionFactory.OpenSession())
            {
                var roleAdapter = session.GetEntityDataAdapter<Role>();
                session.BeginTransaction();

                try
                {
                    foreach (var userId in userIds)
                    {
                        roleAdapter.DissaciateRoleWithUser(new User() { Id = userId }, new Role() { Id = roleId });
                    }

                    session.CommitTransaction();
                }
                catch (Exception exception)
                {
                    session.RollbackTransaction();
                    throw new GoliathException($"Error while associating user to role: {roleId}", exception);
                }
            }
        }

    }
}
