using System.Collections.Generic;
using Goliath.Models;
using Goliath.Web.Authorization;

namespace Goliath.Cms.Services
{
    /// <summary>
    /// 
    /// </summary>
    public partial interface IRoleService
    {
        /// <summary>
        /// Gets all users in role.
        /// </summary>
        /// <param name="roleId">The role identifier.</param>
        /// <param name="limit">The limit.</param>
        /// <param name="offset">The offset.</param>
        /// <param name="total">The total.</param>
        /// <returns></returns>
        ICollection<UserSession> GetAllUsersInRole(long roleId, int limit, int offset, out long total);

        /// <summary>
        /// Gets all users in role.
        /// </summary>
        /// <param name="roleId">The role identifier.</param>
        /// <param name="limit">The limit.</param>
        /// <param name="offset">The offset.</param>
        /// <returns></returns>
        ICollection<UserSession> GetAllUsersInRole(long roleId, int limit, int offset);

        /// <summary>
        /// Gets all permissions.
        /// </summary>
        /// <param name="roleNumber">The role number.</param>
        /// <returns></returns>
        IDictionary<int, PermissionActionModel> GetAllPermissions(int roleNumber);

        /// <summary>
        /// Associates the user to role.
        /// </summary>
        /// <param name="roleId">The role identifier.</param>
        /// <param name="userIds">The user ids.</param>
        void AssociateUserToRole(long roleId, params long[] userIds);

        /// <summary>
        /// Associates the user to role.
        /// </summary>
        /// <param name="roleId">The role identifier.</param>
        /// <param name="userIds">The user ids.</param>
        void DisassociateUser(long roleId, params long[] userIds);
    }
}