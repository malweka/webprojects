﻿using System.Collections.Generic;
using Goliath.Cms.Models;
using Goliath.Web.Authorization;

namespace Goliath.Cms.Services
{
    public interface ILdapAdapter
    {
        string RootDn { get; }

        string UserContainerDn { get; }

        string Host { get; }

        int PortNumber { get; }

        /// <summary>
        /// Authenticates the specified user.
        /// </summary>
        /// <param name="userDn">The user distinguished name.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        bool Authenticate(string userDn, string password);

        /// <summary>
        /// Finds all groups.
        /// </summary>
        /// <param name="userDn">The user distinguished name.</param>
        /// <param name="password">The password.</param>
        /// <param name="containerDn">The container dn.</param>
        /// <param name="filter">The filter.</param>
        /// <returns></returns>
        IList<LdapGroupModel> FindAllGroups(string userDn, string password, string containerDn, string filter);

        /// <summary>
        /// Finds all members of group.
        /// </summary>
        /// <param name="userDn">The user distinguished name.</param>
        /// <param name="password">The password.</param>
        /// <param name="containerDn">The container distinguished name.</param>
        /// <param name="groupName">Name of the group.</param>
        /// <returns></returns>
        IList<UserSession> FindAllMembersOfGroup(string userDn, string password, string containerDn, string groupName);

        /// <summary>
        /// Finds all users.
        /// </summary>
        /// <param name="userDn">The user distinguished name.</param>
        /// <param name="password">The password.</param>
        /// <param name="containerDn">The container distinguished name.</param>
        /// <param name="filter">The filter.</param>
        /// <returns></returns>
        IList<UserSession> FindAllUsers(string userDn, string password, string containerDn, string filter);
    }
}