using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using Goliath.Cms.Data;
using Goliath.Data;
using Goliath.Web;
using Goliath.Web.Authorization;

namespace Goliath.Cms.Services
{
    public class PermissionDataAdapter : IPermissionDataAdapter
    {
        readonly IDatabaseProvider dbProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="PermissionDataAdapter"/> class.
        /// </summary>
        /// <param name="dbProvider">The database provider.</param>
        public PermissionDataAdapter(IDatabaseProvider dbProvider)
        {
            this.dbProvider = dbProvider;
        }

        public IPermissionItem CreateNew(int resourceId, int roleNumber, int permValue)
        {
            return new UserRolePerm() { ResourceId = resourceId, RoleNumber = roleNumber, PermValue = permValue };
        }

        /// <summary>
        /// Inserts the specified resource identifier.
        /// </summary>
        /// <param name="resourceId">The resource identifier.</param>
        /// <param name="roleNumber">The role number.</param>
        /// <param name="permValue">The perm value.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public IPermissionItem Insert(int resourceId, int roleNumber, int permValue, ApplicationContext context = null)
        {
            using (var session = dbProvider.SessionFactory.OpenSession())
            {
                var permItem = new UserRolePerm() { ResourceId = resourceId, RoleNumber = roleNumber, PermValue = permValue, CreatedOn = DateTime.UtcNow};
                if (context != null)
                {
                    permItem.CreatedBy = context.CurrentUser.UserName;
                }
                session.Insert(permItem);
                return permItem;
            }
        }

        /// <summary>
        /// Updates the specified permission item.
        /// </summary>
        /// <param name="permissionItem">The permission item.</param>
        /// <param name="context">The context.</param>
        /// <exception cref="GoliathEntityNotFoundException"></exception>
        public void Update(IPermissionItem permissionItem, ApplicationContext context = null)
        {
            using (var session = dbProvider.SessionFactory.OpenSession())
            {

                var permAdapter = session.GetEntityDataAdapter<UserRolePerm>();
                var item = permAdapter.Select()
                    .Where(c => c.ResourceId).EqualToValue(permissionItem.ResourceId)
                    .And(c => c.RoleNumber).EqualToValue(permissionItem.RoleNumber).FetchOne();

                if (item == null)
                    throw new GoliathEntityNotFoundException($"Could not find an permission for {permissionItem.RoleNumber} on resource ID {permissionItem.ResourceId}");

                item.PermValue = permissionItem.PermValue;
                if (context != null && item.IsDirty)
                {
                    item.ModifiedOn = DateTime.UtcNow;
                    item.ModifiedBy = context.CurrentUser.UserName;
                }
                permAdapter.Update(item);
            }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns></returns>
        public ICollection<IPermissionItem> GetAll()
        {
            using (var session = dbProvider.SessionFactory.OpenSession())
            {
                var permAdapter = session.GetEntityDataAdapter<UserRolePerm>();
                var permissionItemList = new List<IPermissionItem>();
                var permissions = permAdapter.FetchAll();
                foreach (var userRolePerm in permissions)
                {
                    permissionItemList.Add(userRolePerm);
                }

                return permissionItemList;
            }
        }

        /// <summary>
        /// Gets the permission.
        /// </summary>
        /// <param name="resourceId">The resource identifier.</param>
        /// <param name="roleNumber">The role number.</param>
        /// <returns></returns>
        public IPermissionItem GetPermission(int resourceId, int roleNumber)
        {
            using (var session = dbProvider.SessionFactory.OpenSession())
            {
                var permAdapter = session.GetEntityDataAdapter<UserRolePerm>();
                var permissionItem = permAdapter.Select().Where(c => c.RoleNumber).EqualToValue(roleNumber)
                      .And(c => c.ResourceId).EqualToValue(resourceId).FetchOne();
                return permissionItem;

            }
        }

        /// <summary>
        /// Gets the permissions.
        /// </summary>
        /// <param name="resourceId">The resource identifier.</param>
        /// <returns></returns>
        public PermissionList GetPermissions(int resourceId)
        {
            var list = new PermissionList();
            using (var session = dbProvider.SessionFactory.OpenSession())
            {
                var permAdapter = session.GetEntityDataAdapter<UserRolePerm>();
                var items = permAdapter.Select().Where(c => c.ResourceId).EqualToValue(resourceId).FetchAll();
                foreach (var userRolePerm in items)
                {
                    list.Add(userRolePerm);
                }
                return list;

            }
        }

        public void BatchProcess(IList<IPermissionItem> inserts, IList<IPermissionItem> updates, IList<IPermissionItem> deletes, ApplicationContext context = null)
        {
            using (var session = dbProvider.SessionFactory.OpenSession())
            {
                try
                {
                    session.BeginTransaction();

                    var dataAdapter = session.GetEntityDataAdapter<UserRolePerm>();

                    foreach (var item in inserts)
                    {
                        var perm = new UserRolePerm()
                        {
                            ResourceId = item.ResourceId,
                            RoleNumber = item.RoleNumber,
                            PermValue = item.PermValue,
                            CreatedOn = DateTime.UtcNow,
                        };

                        if (context != null)
                        {
                            perm.CreatedBy = context.CurrentUser.UserName;
                        }

                        dataAdapter.Insert(perm);
                    }

                    foreach (var item in updates)
                    {
                        var userRolePerm = item as UserRolePerm;
                        if (userRolePerm == null) continue;

                        if (context != null && userRolePerm.IsDirty)
                        {
                            userRolePerm.ModifiedBy = context.CurrentUser.UserName;
                            userRolePerm.ModifiedOn = DateTime.UtcNow;
                        }
                        dataAdapter.Update(userRolePerm);
                    }

                    foreach (var item in deletes)
                    {
                        var userRolePerm = item as UserRolePerm;
                        if (userRolePerm == null) continue;

                        dataAdapter.Delete(userRolePerm);
                    }

                    session.CommitTransaction();
                }
                catch (Exception exception)
                {
                    session.RollbackTransaction();
                    throw new GoliathDataException("Could not update permissions. Database transaction failed.", exception);
                }
            }
        }

    }
}