GoliathData -generateEntities -namespace="Goliath.Cms.Data"^
 -templateFolder="%~dp0Templates"^
 -workingFolder="%~dp0GoliathCmsBusiness\Data\Generated"^
 -m="%~dp0GoliathCmsHosting\data.map.config"
 
GoliathData -generate -in="EntityResource.razt" -out="EntityResources.resx" -namespace="Goliath.Cms.Data"^
 -templateFolder="%~dp0Templates"^
 -workingFolder="%~dp0GoliathCmsBusiness\Data\Generated"^
 -m="%~dp0GoliathCmsHosting\data.map.config"
 
GoliathData -generate -in="DataAccessExtensions.razt"^
 -out="DataAccessExtensions.cs" -namespace="Goliath.Cms.Data"^
 -templateFolder="%~dp0Templates"^
 -workingFolder="%~dp0GoliathCmsBusiness\Data\Generated"^
 -m="%~dp0GoliathCmsHosting\data.map.config"
 
GoliathData -generateAll -in="EntityValidator.razt" -out="Validator.cs" -namespace="Goliath.Cms.Data"^
 -templateFolder="%~dp0Templates"^
 -workingFolder="%~dp0GoliathCmsBusiness\Data\Generated"^
 -m="%~dp0GoliathCmsHosting\data.map.config" 
 
 GoliathData -generateAll -in="Dto.razt" -out="(name).cs" -namespace="Goliath.Cms.Dtos"^
 -templateFolder="%~dp0Templates"^
 -workingFolder="%~dp0GoliathCmsBusiness\Dtos\Generated"^
 -m="%~dp0GoliathCmsHosting\data.map.config"
 
pause
