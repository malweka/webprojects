GoliathData -namespace="Goliath.Cms.Data"^
 -workingFolder="%~dp0GoliathCmsBusiness\Data\Generated"^
 -baseModel="%~dp0baseModel.xml"^
 -exclude="acct*|dtproperties"^
 -assembly="UserManagement.Core.Data"^
 -connectionstring="Data Source=localhost;Initial Catalog=GoliathCms;Integrated Security=True"^
 -defaultKeygen="Cms_Integer_keyGen"^
 -extension="%~dp0extensions.txt"^
 -complexTypeMap="%~dp0ComplexTypeMap.txt"^
 -renameConfig="%~dp0Renames.txt"^
 -statementMap="%~dp0GoliathCmsBusiness\Data\mappedStatements.xml"
 
 
 copy /Y "%~dp0GoliathCmsBusiness\Data\Generated\data.map.xml" "%~dp0GoliathCmsHosting\data.map.config"
