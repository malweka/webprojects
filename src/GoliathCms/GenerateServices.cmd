GoliathData -generate -in="ResourceTypeMap.razt" -out="ResourceEntityMap.cs" -namespace="Goliath.Cms.Services"^
 -templateFolder="%~dp0Templates"^
 -workingFolder="%~dp0GoliathCmsBusiness\Services\Generated"^
 -m="%~dp0GoliathCmsHosting\data.map.config"
 
 GoliathData -generateAll -in="CrudServiceInterface.razt" -out="I(name)Service.cs" -namespace="Goliath.Cms.Services"^
 -templateFolder="%~dp0Templates"^
 -workingFolder="%~dp0GoliathCmsBusiness\Services\Generated"^
 -m="%~dp0GoliathCmsHosting\data.map.config"
 
  GoliathData -generateAll -in="CrudServiceImpl.razt" -out="(name)Service.cs" -namespace="Goliath.Cms.Services"^
 -templateFolder="%~dp0Templates"^
 -workingFolder="%~dp0GoliathCmsBusiness\Services\Generated"^
 -m="%~dp0GoliathCmsHosting\data.map.config"
 
 GoliathData -generate -in="DataServiceExtensions.razt"^
 -out="DataServiceExtensions.cs" -namespace="Goliath.Cms.Services"^
 -templateFolder="%~dp0Templates"^
 -workingFolder="%~dp0GoliathCmsBusiness\Services\Generated"^
 -m="%~dp0GoliathCmsHosting\data.map.config"
 
pause
