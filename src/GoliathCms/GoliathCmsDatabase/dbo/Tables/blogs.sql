﻿CREATE TABLE [dbo].[blogs] (
    [id]          BIGINT         NOT NULL,
    [name]        NVARCHAR (150) NULL,
    [title]       NVARCHAR (250) NULL,
    [description] NVARCHAR (500) NULL,
    CONSTRAINT [PK_blogs] PRIMARY KEY CLUSTERED ([id] ASC)
);

