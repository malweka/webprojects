﻿CREATE TABLE [dbo].[blog_entries_categories] (
    [entry_id]    BIGINT NOT NULL,
    [category_id] BIGINT NOT NULL,
    CONSTRAINT [PK_blog_entries_categories] PRIMARY KEY CLUSTERED ([entry_id] ASC, [category_id] ASC),
    CONSTRAINT [FK_blog_entries_categories_blog_categories] FOREIGN KEY ([category_id]) REFERENCES [dbo].[blog_categories] ([id]),
    CONSTRAINT [FK_blog_entries_categories_blog_entries] FOREIGN KEY ([entry_id]) REFERENCES [dbo].[blog_entries] ([id])
);

