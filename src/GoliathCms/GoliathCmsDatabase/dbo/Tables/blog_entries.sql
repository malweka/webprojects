﻿CREATE TABLE [dbo].[blog_entries] (
    [id]                  BIGINT         NOT NULL,
    [url_slug]            VARCHAR (250)  NOT NULL,
    [status]              INT            CONSTRAINT [DF_blog_entries_status] DEFAULT ((0)) NOT NULL,
    [title]               NVARCHAR (250) NULL,
    [teaser]              NVARCHAR (500) NULL,
    [blog_text]           NTEXT          NULL,
    [created_on]          DATETIME       NOT NULL,
    [created_by]          VARCHAR (50)   NOT NULL,
    [modified_on]         DATETIME       NULL,
    [modified_by]         VARCHAR (50)   NULL,
    [top_image_uri]       VARCHAR (500)  NULL,
    [top_image_small_uri] VARCHAR (500)  NULL,
    [published_on]        DATETIME       NULL,
    CONSTRAINT [PK_blog_entries] PRIMARY KEY CLUSTERED ([id] ASC)
);

