﻿CREATE TABLE [dbo].[user_accounts] (
    [id]                      BIGINT         NOT NULL,
    [user_name]               NVARCHAR (50)  NULL,
    [first_name]              NVARCHAR (50)  NULL,
    [last_name]               NVARCHAR (50)  NOT NULL,
    [email_address]           NVARCHAR (150) NOT NULL,
    [password_hash]           VARCHAR (250)  NULL,
    [access_token]            VARCHAR (500)  NULL,
    [external_screen_name]    VARCHAR (50)   NULL,
    [external_user_id]        VARCHAR (50)   NULL,
    [authentication_provider] VARCHAR (50)   NULL,
    [is_active]               BIT            CONSTRAINT [DF_user_accounts_is_active] DEFAULT ((1)) NOT NULL,
    [is_domain_user]          BIT            CONSTRAINT [DF_user_accounts_is_domain_user] DEFAULT ((0)) NOT NULL,
    [use_gravatar_profile]    BIT            CONSTRAINT [DF_user_accounts_use_gravatar_profile] DEFAULT ((1)) NOT NULL,
    [profile_image_url]       NVARCHAR (250) NULL,
    [last_login_on]           DATETIME       NULL,
    [created_on]              DATETIME       CONSTRAINT [DF_user_accounts_created_on] DEFAULT (getutcdate()) NOT NULL,
    [created_by]              VARCHAR (50)   CONSTRAINT [DF_user_accounts_created_by] DEFAULT ('System') NOT NULL,
    [modified_on]             DATETIME       NULL,
    [modified_by]             VARCHAR (50)   NULL,
    CONSTRAINT [PK_users] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [IX_users] UNIQUE NONCLUSTERED ([user_name] ASC)
);



