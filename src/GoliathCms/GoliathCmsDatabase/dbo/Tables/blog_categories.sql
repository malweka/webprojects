﻿CREATE TABLE [dbo].[blog_categories] (
    [id]           BIGINT        NOT NULL,
    [name]         NVARCHAR (50) NOT NULL,
    [display_name] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_blog_categories] PRIMARY KEY CLUSTERED ([id] ASC)
);

