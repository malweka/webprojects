﻿CREATE TABLE [dbo].[app_resources] (
    [id]           BIGINT        NOT NULL,
    [name]         VARCHAR (50)  NOT NULL,
    [display_name] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Resource] PRIMARY KEY CLUSTERED ([id] ASC)
);

