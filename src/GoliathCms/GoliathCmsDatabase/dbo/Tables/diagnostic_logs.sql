﻿CREATE TABLE [dbo].[diagnostic_logs] (
    [id]            BIGINT          NOT NULL,
    [session_id]    VARCHAR (50)    NULL,
    [log_level]     VARCHAR (20)    CONSTRAINT [DF_diagnostic_logs_log_level] DEFAULT ('DEBUG') NOT NULL,
    [log_message]   NVARCHAR (2000) NULL,
    [log_exception] NVARCHAR (4000) NULL,
    [created_on]    DATETIME        CONSTRAINT [DF_diagnostic_logs_created_on] DEFAULT (getutcdate()) NOT NULL,
    CONSTRAINT [PK_diagnostic_logs] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Created on date and time', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'diagnostic_logs', @level2type = N'COLUMN', @level2name = N'created_on';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Exception message', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'diagnostic_logs', @level2type = N'COLUMN', @level2name = N'log_exception';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Log message', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'diagnostic_logs', @level2type = N'COLUMN', @level2name = N'log_message';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Log level such as DEBUG, INFO, WARN with DEBUG being more verbous than WARN', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'diagnostic_logs', @level2type = N'COLUMN', @level2name = N'log_level';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'User session Identifier', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'diagnostic_logs', @level2type = N'COLUMN', @level2name = N'session_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Application diagnostic logs', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'diagnostic_logs';

