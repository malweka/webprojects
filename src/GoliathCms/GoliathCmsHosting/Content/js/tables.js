﻿function GoTable(opts) {

    var prx = {
        url: opts.url,
        limit: opts.limit,
        offset: opts.offset,
        currentPage: opts.currentPage,
        totalPages: opts.totalPages,
        columns: opts.columns,
        isPreloaded: opts.isPreloaded,
        skipFirstColumn: opts.skipFirstColumn,
        sortColumn: opts.sortColumn,
        formatColumn: opts.formatColumn,
        firstColumnFormat: opts.firstColumnFormat
    }

    if (prx.isPreloaded != null) {
        this.isPreloaded = prx.isPreloaded;
    } else {
        this.isPreloaded = false;
    }
    if (prx.skipFirstColumn != null) {
        this.skipFirstColumn = prx.skipFirstColumn;
    } else {
        this.skipFirstColumn = true;
    }

    if (prx.formatColumn == null) {
        prx.formatColumn = function (colname, colValue) {
            if (colValue == null) {
                return " ";
            } else {
                return colValue;
            }
        };
    }

    this.firstColumnEmpty = function (itemName, dataItem) {
        return "<td> </td>";
    }


    if (prx.firstColumnFormat == null) {
        prx.firstColumnFormat = this.firstColumnEmpty;
    }

    this.paginate = function (jqObj) {
        prx.selector = jqObj.selector;
        prx.pagerSelector = jqObj.selector + "_pager";

        if (prx.totalPages > 1) {
            if ($(prx.pagerSelector).length < 1) {
                var div = $(prx.selector).parent().parent();

                div.append('<div class="row" id="' + prx.pagerSelector.replace("#", "") + '"><div id="' + prx.pagerSelector.replace("#", "") + '_disp" class="col-sm-3">'
                    + '</div>'
                    + '<div class="col-sm-7"><nav><ul class="pager">'
                    + '<li><a href="#" id="' + prx.pagerSelector.replace("#", "") + '_prev">Previous</a> </li>'
                     + '<li><a href="#" id="' + prx.pagerSelector.replace("#", "") + '_nxt">Next</a> </li>'
                    + "</ul></nav></div></div>");

                $(prx.pagerSelector + "_prev").click(function () {
                    var nxtPage = prx.currentPage - 1;
                    if (nxtPage < 1)
                        return;
                    prx.currentPage = nxtPage;
                    prx.offset = (nxtPage * prx.limit) - prx.limit;
                    prx.loadDataFx(jqObj);
                });

                $(prx.pagerSelector + "_nxt").click(function () {
                    var nxtPage = prx.currentPage + 1;
                    if (nxtPage > prx.totalPages) {
                        return;
                    }

                    prx.currentPage = nxtPage;
                    prx.offset = (nxtPage * prx.limit) - prx.limit;

                    prx.loadDataFx(jqObj);
                });

                checkPager(prx.pagerSelector);
            }
        }


    }

    this.loadData = function (jqObj) {
        prx.selector = jqObj.selector;
        $.ajax({
            crossDomain: true,
            url: prx.url + "?limit=" + prx.limit + "&offset=" + prx.offset + "&pages=" + prx.totalPages + "&srt=" + prx.sortColumn,
            mimeType: 'application/json',
            error: onError,
            success: onSuccess
        });
    }

    prx.loadDataFx = this.loadData;
    function onSuccess(data, textStatus, jqXhr) {
        var tblBody = $(prx.selector).find("tbody").empty();
        for (var i = 0; i < data.items.length; i++) {
            var row = "<tr>";
            var item = data.items[i];
            if (prx.skipFirstColumn) {
                row += prx.firstColumnFormat(data.title, item);
            }

            for (var c = 0; c < prx.columns.length; c++) {
                if (item.hasOwnProperty(prx.columns[c])) {
                    var colValue = prx.formatColumn(prx.columns[c], item[prx.columns[c]]);
                    row += ("<td>" + colValue + "</td>");
                } else {
                    row += "<td> </td>";
                }
            }
            row += "</tr>";
            tblBody.append(row);
        }

        checkPager(prx.selector + "_pager");
        prx.totalPages = data.totalPages;
    }

    function onError(jqXhr, textStatus, errorThrown) {
        alert(textStatus + " " + errorThrown);
    }

    function checkPager(selector) {
        var nxt = $(selector + '_nxt');
        var prev = $(selector + '_prev');
        var disp = $(selector + '_disp');

        if (prx.currentPage >= prx.totalPages) {
            nxt.hide();
        } else {
            nxt.show();
        }

        if (prx.currentPage <= 1) {
            prev.hide();
        } else {
            prev.show();
        }
        disp.empty();
        disp.append("Showing page " + prx.currentPage + " of " + prx.totalPages);
    }

}

function makeFirstColumnCheckbox(itemName, dataItem) {
    if (dataItem == null) {
        return "";
    }
    var elmName = itemName + dataItem.id;
    return '<td><input type="checkbox" id="' + elmName + '" name="' + elmName + '" value="' + dataItem.id + ')" /> </td>';
}

(function ($) {

    $.fn.goTable = function (opts) {

        var dt = new GoTable(opts);
        dt.paginate(this);
        if (!dt.isPreloaded)
            dt.loadData(this);

        return this;
    };

}(jQuery));