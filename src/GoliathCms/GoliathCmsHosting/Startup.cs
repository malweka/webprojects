using Autofac;
using Goliath.Cms.Services;
using Goliath.Data;
using Goliath.Security;
using Goliath.Web;
using Goliath.Web.Authorization;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.Owin.Security.Twitter;
using Nancy.Owin;
using Owin;

namespace Goliath.Cms
{
    public class Startup
    {
        static readonly DatabaseProvider dbProvider = new DatabaseProvider();

        public void Configuration(IAppBuilder app)
        {
            dbProvider.Init();

            var builder = CreateContainerBuilder();
            var container = builder.Build();

            SetUpAuth(app, container);
            SetUpNancy(app, container);
#if DEBUG
            var cache = container.Resolve<ICacheProvider>();
            cache.ReleaseAll();
#endif
            var permissionCache = container.Resolve<IPermissionStore>();
            permissionCache.Load();
        }

        void SetUpAuth(IAppBuilder app, IContainer container)
        {

            var consumerKey = settingProvider.GetAppSettingValue("twitterConsumerKey");
            var consumerSecret = settingProvider.GetAppSettingValue("twitterConsumerSecret");
            var cookieName = settingProvider.GetAppSettingValue("authCookieName");

            app.CreatePerOwinContext<GoliathUserManager>((options, context) =>
            {
                var uman = container.Resolve<UserManager<UserSession, long>>();
                return uman as GoliathUserManager;
            });

            app.CreatePerOwinContext<GoliathSignInManager>((options, context) =>
            {
                var uman = container.Resolve<UserManager<UserSession, long>>();
                return new GoliathSignInManager(uman, context.Authentication);
            });

            app.UseCookieAuthentication(new Microsoft.Owin.Security.Cookies.CookieAuthenticationOptions()
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/auth/login"),
                CookieHttpOnly = true,
                CookieName = cookieName,
                //ExpireTimeSpan = 
           
            });

            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);
            var twitterOptions = new TwitterAuthenticationOptions()
            {
                AuthenticationType = "Twitter",
                ConsumerKey = consumerKey,
                ConsumerSecret = consumerSecret,
                CallbackPath = new PathString("/sauth/callback"),
                BackchannelCertificateValidator = new CertificateSubjectKeyIdentifierValidator(new[]
                {
                    "A5EF0B11CEC04103A34A659048B21CE0572D7D47", // VeriSign Class 3 Secure Server CA - G2
                    "0D445C165344C1827E1D20AB25F40163D8BE79A5", // VeriSign Class 3 Secure Server CA - G3
                    "7FD365A7C2DDECBBF03009F34339FA02AF333133", // VeriSign Class 3 Public Primary Certification Authority - G5
                    "39A55D933676616E73A761DFA16A7E59CDE66FAD", // Symantec Class 3 Secure Server CA - G4
                    "5168FF90AF0207753CCCD9656462A212B859723B",
                    "B13EC36903F8BF4701D498261A0802EF63642BC3"
                })
            };

            

            app.UseTwitterAuthentication(twitterOptions);
            //app.UseGoogleAuthentication();
            app.UseNancyDataProtectorProvider(container);
        }

        void SetUpNancy(IAppBuilder app, IContainer container)
        {
            var bootsrapper = new Bootstrapper(container);
            var nancyOptions = new NancyOptions { Bootstrapper = bootsrapper };
            app.UseNancy(nancyOptions);
        }

        ApplicationSettingProvider settingProvider = new ApplicationSettingProvider();

        ContainerBuilder CreateContainerBuilder()
        {
            var builder = new ContainerBuilder();

            builder.RegisterInstance(dbProvider).As<IDatabaseProvider>();
            builder.RegisterType<ApplicationSettingProvider>().AsSelf();
            builder.RegisterType<InMemoryCachedSettingsProvider>().As<ISettingsProvider>();
            builder.RegisterType<InMemoryCache>().As<ICacheProvider>();
            builder.RegisterType<DefaultContext>().As<ApplicationContext>();

            builder.RegisterModule(new DataServiceModule());
            builder.RegisterModule(new SecurityModule());
            builder.RegisterInstance(settingProvider);

            return builder;
        }
    }

    public static class AppBuilderExtensions
    {
        public static void UseNancyDataProtectorProvider(this IAppBuilder appBuilder, IContainer container)
        {
            var provider = container.Resolve<IDataProtectionProvider>();
            appBuilder.SetDataProtectionProvider(provider);
        }
    }

}