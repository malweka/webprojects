﻿

using System;
#if DEBUG
using System;
using ServiceHost.Debugger;
#else
using System.ServiceProcess;
#endif
using Microsoft.Owin.Hosting;
using Microsoft.Owin.Security;
using Owin;

namespace Goliath.Cms
{
    public partial class CmsServiceHost : ServiceBase
    {
        private IDisposable apiService;
        private bool started;

        public CmsServiceHost()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            //netsh http add urlacl url=http://+:8899/ user=DOMAIN\user
            var uri = System.Configuration.ConfigurationManager.AppSettings.Get("applicationUrl") ?? "http://+:8690";
            apiService = WebApp.Start<Startup>(uri);
            started = true;
        }

        protected override void OnStop()
        {
            if (!started) return;

            apiService.Dispose();
            started = false;
        }
    }
}
