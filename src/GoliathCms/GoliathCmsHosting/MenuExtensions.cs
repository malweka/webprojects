using System;
using System.Collections.Generic;
using System.Text;
using Goliath.Cms.Services;
using Goliath.Models;
using Goliath.Web.Authorization;
using Nancy.ViewEngines;
using Nancy.ViewEngines.Razor;

namespace Goliath.Cms
{
    public static class MenuExtensions
    {
        static MenuModel sideMenu = new MenuModel();
        private static bool isSideMenuLoaded;

        /// <summary>
        /// Sides the menu.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="helpers">The helpers.</param>
        /// <param name="user">The user.</param>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        public static IHtmlString SideMenu<T>(this HtmlHelpers<T> helpers, UserSession user, ViewModel model)
        {
            var menu = GetSideMenuModel();

            if(model == null)
                model = new DisplayViewModel();

            StringBuilder sb = new StringBuilder("<ul class=\"nav navbar-nav\">");
            var permValidator = user.GetPermissionValidator();

            foreach (var menuModel in menu.SubMenus)
            {
                if (!string.IsNullOrWhiteSpace(menuModel.ResourceName) 
                    && !string.IsNullOrWhiteSpace(menuModel.PermissionAction) 
                    && !CanShow(permValidator, menuModel.ResourceName, menuModel.PermissionAction))
                {
                    continue;
                }

                sb.AppendLine(PrintSideMenuItem(menuModel, helpers.RenderContext, model.TabName));
            }

            sb.AppendLine("</ul>");
            var markup = sb.ToString();
            return new NonEncodedHtmlString(markup);
        }

        public static IHtmlString ControlPanel<T>(this HtmlHelpers<T> helpers, UserSession user, string selected, string menuFile)
        {
            var menu = GetMenu(menuFile);
            var permValidator = user.GetPermissionValidator();

            StringBuilder sb = new StringBuilder("<ul class=\"nav\">");
            foreach (var menuModel in menu.SubMenus)
            {
                if (!string.IsNullOrWhiteSpace(menuModel.ResourceName)
                    && !string.IsNullOrWhiteSpace(menuModel.PermissionAction)
                    && !CanShow(permValidator, menuModel.ResourceName, menuModel.PermissionAction))
                {
                    continue;
                }

                sb.AppendLine(PrintPanelItem(menuModel, helpers.RenderContext, selected));
            }

            sb.AppendLine("</ul>");
            var markup = sb.ToString();
            return new NonEncodedHtmlString(markup);
        }

        

        static bool CanShow(PermissionValidator permValidator, string resourceName, string permAction)
        {
            int resourceId;
            int actionId;
            if (ResourcePermissionGroupMapper.PermissionResource.PermissionGroups.TryGetValue(resourceName, out resourceId) && PermissionActions.ActionList.TryGetValue(permAction, out actionId))
                return permValidator.CanPerformAction(resourceId, actionId);

            else return false;
        }

        static MenuModel GetSideMenuModel()
        {
            if (isSideMenuLoaded)
                return sideMenu;

            sideMenu = GetMenu("SideMenu");
            isSideMenuLoaded = true;
            return sideMenu;
        }

        static MenuModel GetMenu(string menuFilePath)
        {
            var filepath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin", string.Empty), string.Concat(menuFilePath, ".config"));

            var menu = MenuModel.LoadFromFile(filepath);
            return menu;
        }

        static string PrintSideMenuItem(MenuModel menu, IRenderContext renderContext, string selectedMenu)
        {
            string selectedClass = string.Empty;
            bool hasSubMenu = false;

            if (menu.Name.Equals(selectedMenu))
            {
                selectedClass = "active ";
            }

            StringBuilder sb = new StringBuilder();

            sb.Append(string.Concat("<li class=\"", selectedClass, " ", menu.CssClasses, "\"><a href=\"", renderContext.ParsePath(menu.Source), "\">"));
            sb.AppendLine(string.Concat("<span class=\"small-nav\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"", menu.DisplayName, "\">"));
            sb.AppendLine(string.Concat("<i class=\"", menu.IconUrl, "\" aria-hidden=\"true\"></i></span>"));
            sb.AppendLine(string.Concat("<span class=\"full-nav\"><i style='margin-right: 10px;' class=\"", menu.IconUrl, "\" aria-hidden=\"true\"></i> ", menu.DisplayName," </span>"));
            sb.Append("</a></li>");

            return sb.ToString();
        }

        static string PrintPanelItem(MenuModel menu, IRenderContext renderContext, string selectedMenu)
        {
            string selectedClass = string.Empty;
            bool hasSubMenu = false;
            if (menu.Name.Equals(selectedMenu))
            {
                selectedClass = "current ";
            }
            if (menu.SubMenus.Count > 0)
            {
                selectedClass = selectedClass + "submenu";
                hasSubMenu = true;
            }

            StringBuilder sb = new StringBuilder();
            string menuHtml = string.Concat("<li class=\"", selectedClass,
                "\"><a href=\"", renderContext.ParsePath(menu.Source), "\"><i class=\"", menu.IconUrl, "\"></i> ", menu.DisplayName);

            sb.Append(menuHtml);
            if (hasSubMenu)
            {
                sb.Append("<span class=\"caret pull - right\"></span></a>");
                sb.AppendLine("<ul>");
                foreach (var subMen in menu.SubMenus)
                {
                    sb.AppendFormat("<li><a href=\"{0}\"><i class=\"{1}\"></i> {2}</a></li>", renderContext.ParsePath(subMen.Source), subMen.IconUrl, subMen.DisplayName);
                }
                sb.AppendLine("</ul></li>");
            }
            else
            {
                sb.Append("</a></li>");
            }

            return sb.ToString();
        }
    }
}