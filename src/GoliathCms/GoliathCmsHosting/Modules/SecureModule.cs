﻿using Nancy;
using Nancy.Security;

namespace Goliath.Cms.Modules
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Nancy.NancyModule" />
    public abstract class SecureModule : NancyModule
    {

        protected SecureModule()
        {
            this.RequiresMSOwinAuthentication();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SecureModule"/> class.
        /// </summary>
        /// <param name="modulePath">A <see cref="T:System.String" /> containing the root relative path that all paths in the module will be a subset of.</param>
        protected SecureModule(string modulePath) : base(modulePath)
        {
            this.RequiresMSOwinAuthentication();
        }
    }
}