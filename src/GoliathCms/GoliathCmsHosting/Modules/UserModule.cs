﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goliath.Cms.Data;
using Goliath.Cms.Models;
using Goliath.Cms.Services;
using Goliath.Models;
using Nancy;

namespace Goliath.Cms.Modules
{
    public class UserModule : SecureModule
    {
        private readonly IUserService userService;

        public UserModule(IUserService userService):base("user")
        {
            this.userService = userService;
            Get["/modal/"] = ListUsersModal;
        }

        dynamic ListUsersModal(dynamic parameters)
        {

            long total;
            var users = userService.FetchAll(ApiBaseModule.Limit, 0, out total, null, PropertyNames.User.LastName);
            var model = new ListViewModel<User>() { TabName = "Access", Title = "User Access Control", Items = users.ToList()};
            model.TotalPages = (int)Math.Ceiling((double)total / ApiBaseModule.Limit);
            model.CurrentPage = 1;
           

            return Negotiate.WithModel(model).WithView("modal_list");
        }
    }
}
