﻿using System;
using System.Collections.Generic;
using System.Linq;
using Goliath.Cms.Data;
using Goliath.Cms.Models;
using Goliath.Cms.Services;
using Goliath.Data.Diagnostics;
using Goliath.Models;
using Goliath.Web;
using Goliath.Web.Authorization;
using Nancy;
using Nancy.ModelBinding;

namespace Goliath.Cms.Modules
{
    public class SecurityModule : SecureModule
    {
        private readonly IUserService userService;
        private readonly IUserAccessLogService accessLogService;
        private readonly IRoleService roleService;
        private readonly IPermissionStore permissionStore;
        static readonly ILogger logger;

        static SecurityModule()
        {
            logger = Logger.GetLogger(typeof(SecurityModule));
        }

        public SecurityModule(IUserService userService, IUserAccessLogService accessLogService,
            IRoleService roleService, IPermissionStore permissionStore) : base("/security")
        {
            this.userService = userService;
            this.accessLogService = accessLogService;
            this.roleService = roleService;
            this.permissionStore = permissionStore;

            Get["/"] = ListUsers;
            Get["/roles"] = ListRoles;
            Get["/roles/{roleId}"] = ManageRoleGet;
            Get["/roles/{roleId}/edit"] = RoleEditGet;
            Post["/roles/{roleId}/edit"] = RoleEditPost;
            Post["/roles/{roleId}/permissions"] = SaveRolePermission;
            Post["/roles/addusers"] = AddUsersToRole;
            Get["/roles/{roleId}/removeuser/{userId}"] = RemoveUser;
        }

        dynamic ListUsers(dynamic parameters)
        {
            var data = new SecurityDashboardModel();

            long total;
            var users = userService.FetchAll(ApiBaseModule.Limit, 0, out total, null, PropertyNames.User.LastName);
            data.TotalUserCount = total;
            data.PageCount = (int)Math.Ceiling((double)total / ApiBaseModule.Limit);
            data.Users = users;
            data.AccessLog = accessLogService.FetchAll(ApiBaseModule.Limit, 0, null, string.Concat(PropertyNames.UserAccessLog.CreatedOn, "_DESC"));

            var model = new EditViewModel<SecurityDashboardModel>() { TabName = "Access", Title = "User Access Control", Data = data };

            return Negotiate.WithModel(model).WithView("index");
        }

        dynamic RoleEditGet(dynamic parameters)
        {
            long roleId;

            if (!long.TryParse(parameters.roleId, out roleId))
                return 404;


            var role = roleService.FetchOne(roleId);
            if (role == null)
                return 404;

            var model = new EditViewModel<Role>() { ActionType = ViewActionType.Edit, Id = role.Id, Data = role, Title = "Edit" };

            return View["role_edit", model];
        }

        dynamic RoleEditPost(dynamic parameters)
        {
            long roleId;

            if (!long.TryParse(parameters.roleId, out roleId))
                return 404;

            var saved = this.Bind<Role>();

            var role = roleService.FetchOne(roleId);
            if (role == null || !roleId.Equals(saved.Id))
                return 404;

            role.Description = saved.Description;
            roleService.Update(role);

            var model = new EditViewModel<Role>
            {
                ActionType = ViewActionType.Edit,
                Id = role.Id,
                Data = role,
                Title = "Edit",
                ExecutionInfo = { ExecutionState = ViewExecutionState.Success, Message = "Saved Successfully." }
            };

            return Response.AsRedirect("~/security/roles/" + roleId);
        }

        dynamic RemoveUser(dynamic parameters)
        {
            long roleId;

            if (!long.TryParse(parameters.roleId, out roleId))
                return 404;

            long userId;

            if (!long.TryParse(parameters.userId, out userId))
                return 404;

            roleService.DisassociateUser(roleId, userId);
            return Response.AsRedirect("~/security/roles/" + roleId);
        }

        dynamic AddUsersToRole (dynamic parameters)
        {
            var data = this.Bind<AddUserToRoleModel>();
            roleService.AssociateUserToRole(data.RoleId, data.SelectedIds);
            return Response.AsRedirect("~/security/roles/" + data.RoleId);
        }

        dynamic ListRoles(dynamic parameters)
        {
            var model = new ListViewModel<Role>() { TabName = "Access", Title = "User Access Control" };

            long total;
            var roles = roleService.FetchAll(ApiBaseModule.Limit, 0, out total, null, PropertyNames.Role.Description).ToList();

            model.Items = roles;
            model.TotalPages = (int)Math.Ceiling((double)total / ApiBaseModule.Limit);
            model.CurrentPage = 1;

            return Negotiate.WithModel(model).WithView("RoleList");
        }

        public dynamic ManageRoleGet(dynamic parameters)
        {
            try
            {
                if (!roleService.CurrentContext.AuthorizatonValidator.CanEdit(typeof(Role)))
                    return 403;

                long roleId;

                if (!long.TryParse(parameters.roleId, out roleId))
                    return 404;

                var role = roleService.FetchOne(roleId);
                if (role == null) return 404;

                var model = new EditViewModel<RoleManageModel>
                {
                    Id = role.Id,
                    Description = role.Description,
                    TabName = "Access",
                    Data = new RoleManageModel() { Id = role.Id, RoleName = role.Name }
                };

                model.Data.Users.Items = roleService.GetAllUsersInRole(roleId, 10, 0).ToList();

                var perms = roleService.GetAllPermissions(role.RoleNumber);
                model.Data.ActionList = PermissionActions.ActionList;
                var resources = ResourcePermissionGroupMapper.PermissionResource.GetPermissionResoureList();

                var permissionList = new List<PermissionActionModel>();
                foreach (var appResource in resources)
                {
                    PermissionActionModel pmodel;
                    if (!perms.TryGetValue(appResource.Value, out pmodel))
                    {
                        pmodel = new PermissionActionModel
                        {
                            ResourceId = appResource.Value,
                            ResourceName = appResource.Key,
                            PermValue = 0,
                            RoleNumber = role.RoleNumber
                        };
                    }
                    else
                    {
                        pmodel.ResourceName = appResource.Key;
                    }

                    permissionList.Add(pmodel);
                }

                model.Data.Permissions.Items = permissionList;

                return View["RoleManage", model];

            }
            catch (Exception exception)
            {
                this.LogException(logger, "Couldn't load manage roles", exception);
#if DEBUG
                throw;
#else
                return 500;
#endif

            }
        }

        public dynamic SaveRolePermission(dynamic parameters)
        {
            try
            {
                long roleId;

                if (!roleService.CurrentContext.AuthorizatonValidator.CanEdit(typeof(Role)))
                    return 403;


                if (!long.TryParse(parameters.roleId, out roleId))
                    return 404;

                var role = roleService.FetchOne(roleId);
                if (role == null) return 404;

                var permissions = this.Bind<List<PermissionActionModel>>();
                var form = Request.Form as IDictionary<string, object> ?? new Dictionary<string, object>();
                var actions = PermissionActions.ActionList;
                for (var i = 0; i < permissions.Count; i++)
                {
                    int permVal = 0;
                    foreach (var action in actions)
                    {
                        object updatedValue;
                        string key = $"PermissionActions_{i}[{action.Key}]";
                        if (form.TryGetValue(key, out updatedValue))
                        {
                            var val = Convert.ToInt32(updatedValue);
                            permVal = permVal | val;
                        }
                    }

                    permissions[i].PermValue = permVal;

                }

                permissionStore.UpdateRolePermissions(role, permissions, roleService.CurrentContext);

                return Response.AsRedirect("~/security/roles/" + roleId);

            }
            catch (Exception exception)
            {
                this.LogException(logger, "Couldn't Save Role Permission", exception);
#if DEBUG
                throw;
#else
                return 500;
#endif
            }
        }
    }
}