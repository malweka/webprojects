using System;
using System.Net;
using System.Security.Claims;
using Goliath.Models;
using Goliath.Web.Authorization;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Security;

namespace Goliath.Cms.Modules
{
    public class AuthModule : NancyModule
    {
        private readonly IGoliathUserAccessManager accessManager;
        private readonly ApplicationSettingProvider settingsProvider;

        public AuthModule(IGoliathUserAccessManager accessManager, ApplicationSettingProvider settingsProvider) : base("/auth")
        {
            this.accessManager = accessManager;
            this.settingsProvider = settingsProvider;


            Get["/login"] = parameters =>
            {
                var returnInfo = GetLoginUrl();

                var model = new LoginModel
                {
                    ReturnUrl = returnInfo.ReturnString,
                    HasError = this.Request.Query.error.HasValue
                };

                return Negotiate.WithModel(model)
                    .WithView("login")
                    .WithHeader("Etag", DateTime.UtcNow.Ticks.ToString())
                    .WithHeader("Last-Modified", DateTime.UtcNow.ToString("R"))
                    .WithHeader("Cache-Control", "max-age=0, must-revalidate");
            };

            Post["/login"] = LogInInternalPost;
            Post["/extern"] = LogInExternalPost;
            Get["/extern/{provider}"] = LogInExternalCallback;
            Post["/logoff"] = LogOff;
        }

        ReturnUrlInfo GetLoginUrl()
        {
            string returnUrl = "~/";
            if (Request.Query.ReturnUrl.HasValue)
            {
                returnUrl = Request.Query.ReturnUrl.ToString();
            }

            string returnString = string.Empty;
            if (!string.IsNullOrWhiteSpace(returnUrl))
            {
                returnString = string.Concat("?ReturnUrl=", WebUtility.UrlEncode(returnUrl));
            }

            var loginUrl = $"{settingsProvider.GetAppSettingValue("loginUrl")}{returnString}";

            return new ReturnUrlInfo() { ReturnUrl = returnUrl, LoginUrl = loginUrl, ReturnString = returnString };
        }

        struct ReturnUrlInfo
        {
            public string ReturnUrl;
            public string ReturnString;
            public string LoginUrl;
        }

        dynamic LogInInternalPost(dynamic parameters)
        {
            try
            {
                ReturnUrlInfo loginInfo = GetLoginUrl();
                var loginModel = this.Bind<LoginModel>();
                var auth = Context.GetAuthenticationManager();

                var result = accessManager.AuthenticateLocalUser(loginModel.Username, loginModel.Password, Request.UserHostAddress);
                if (((result.Status & AuthenticationStatus.Failed) == AuthenticationStatus.Failed) || (result.UserSession == null))
                {
                    return Response.AsRedirect(loginInfo.LoginUrl);
                }

                auth.SignOut(new string[] { DefaultAuthenticationTypes.ExternalCookie });
                auth.SignIn(new AuthenticationProperties { IsPersistent = false }, new ClaimsIdentity[] { result.UserSession.CreateClaimsIdentity(DefaultAuthenticationTypes.ApplicationCookie) });

                accessManager.CacheUser(result.UserSession);
                return Response.AsRedirect(loginInfo.ReturnUrl);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        dynamic LogInExternalPost(dynamic parameters)
        {
            var loginModel = this.Bind<ExternalLoginModel>();

            ReturnUrlInfo loginInfo = GetLoginUrl();
            if (!string.IsNullOrWhiteSpace(loginModel.Provider))
            {
                var auth = Context.GetAuthenticationManager();

                auth.Challenge(new AuthenticationProperties()
                {
                    RedirectUri = $"/auth/extern/{loginModel.Provider}{loginInfo.ReturnString}"
                }, loginModel.Provider);

                return 401;
            }

            return 404;
        }

        dynamic LogInExternalCallback(dynamic parameters)
        {
            var auth = Context.GetAuthenticationManager();
            var loginInfo = auth.GetExternalLoginInfo();

            ReturnUrlInfo returnInfo = GetLoginUrl();

            if (loginInfo == null)
            {
                return Response.AsRedirect(returnInfo.LoginUrl);
            }

            if (loginInfo.ExternalIdentity.IsAuthenticated)
            {
                // var result = accessManager.AuthenticateExternalUser(loginInfo.Login.LoginProvider, loginInfo.Login.ProviderKey, Request.UserHostAddress);
                var result = accessManager.AuthenticateExternalUser(loginInfo.Login.LoginProvider, loginInfo.DefaultUserName ?? loginInfo.Login.ProviderKey, Request.UserHostAddress);

                if (((result.Status & AuthenticationStatus.Failed) == AuthenticationStatus.Failed) || (result.UserSession == null))
                {
                    return Response.AsRedirect(returnInfo.LoginUrl);
                }

                auth.SignOut(new string[] { DefaultAuthenticationTypes.ExternalCookie });
                auth.SignIn(new AuthenticationProperties { IsPersistent = false }, new ClaimsIdentity[] { result.UserSession.CreateClaimsIdentity(DefaultAuthenticationTypes.ApplicationCookie) });

                accessManager.CacheUser(result.UserSession);
            }
            else
            {
                return Response.AsRedirect(returnInfo.LoginUrl);
            }

            return Response.AsRedirect(returnInfo.ReturnUrl);
        }

        dynamic LogOff(dynamic parameters)
        {
            var owinUser = Context.GetMSOwinUser();
            if (owinUser == null) return Response.AsRedirect("~/");

            var userId = owinUser.Identity.GetUserId<long>();
            accessManager.CacheForgetUser(userId);

            var auth = Context.GetAuthenticationManager();
            auth.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            return Response.AsRedirect("~/");
        }
    }
}