﻿using Nancy;
using Nancy.Security;

namespace Goliath.Cms.Modules
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class ApiBaseModule : NancyModule
    {
        public const int Limit = 10;

        protected ApiBaseModule(string modulepath)
            : base(modulepath)
        {
            
        }
        /// <summary>
        /// Assesses the security.
        /// </summary>
        protected virtual void AssessSecurity()
        {
            this.RequiresAuthentication();
        }
    }
}