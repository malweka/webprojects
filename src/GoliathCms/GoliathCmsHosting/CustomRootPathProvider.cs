using System;
using Nancy;

namespace Goliath.Cms
{
    /// <summary>
    /// 
    /// </summary>
    public class CustomRootPathProvider : IRootPathProvider
    {
        /// <summary>
        /// Returns the root folder path of the current Nancy application.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String" /> containing the path of the root folder.
        /// </returns>
        public string GetRootPath()
        {
            var path = AppDomain.CurrentDomain.BaseDirectory;

            var newpath = path.Replace(@"\bin", string.Empty);
            return newpath;
        }
    }
}