﻿using System;
using System.Collections.Generic;
using Goliath.Web.Authorization;

namespace Goliath.Ums
{
    public static class PermissionExtensions
    {
        /// <summary>
        /// Determines whether this instance can view the specified resource identifier.
        /// </summary>
        /// <param name="validator">The validator.</param>
        /// <param name="resourceId">The resource identifier.</param>
        /// <returns></returns>
        public static bool CanView(this PermissionValidator validator, int resourceId)
        {
            return validator.CanPerformAction(resourceId, PermissionActions.View);
        }

        /// <summary>
        /// Determines whether this instance can view the specified resource type.
        /// </summary>
        /// <param name="validator">The validator.</param>
        /// <param name="resourceType">Type of the resource.</param>
        /// <returns></returns>
        public static bool CanView(this PermissionValidator validator, Type resourceType)
        {
            return validator.CanPerformAction(resourceType, PermissionActions.View);
        }

        /// <summary>
        /// Determines whether this instance can edit the specified resource identifier.
        /// </summary>
        /// <param name="validator">The validator.</param>
        /// <param name="resourceId">The resource identifier.</param>
        /// <returns></returns>
        public static bool CanEdit(this PermissionValidator validator, int resourceId)
        {
            return validator.CanPerformAction(resourceId, PermissionActions.Edit);
        }

        /// <summary>
        /// Determines whether this instance can edit the specified resource type.
        /// </summary>
        /// <param name="validator">The validator.</param>
        /// <param name="resourceType">Type of the resource.</param>
        /// <returns></returns>
        public static bool CanEdit(this PermissionValidator validator, Type resourceType)
        {
            return validator.CanPerformAction(resourceType, PermissionActions.Edit);
        }

        /// <summary>
        /// Determines whether this instance can create the specified resource identifier.
        /// </summary>
        /// <param name="validator">The validator.</param>
        /// <param name="resourceId">The resource identifier.</param>
        /// <returns></returns>
        public static bool CanCreate(this PermissionValidator validator, int resourceId)
        {
            return validator.CanPerformAction(resourceId, PermissionActions.Create);
        }

        /// <summary>
        /// Determines whether this instance can create the specified resource type.
        /// </summary>
        /// <param name="validator">The validator.</param>
        /// <param name="resourceType">Type of the resource.</param>
        /// <returns></returns>
        public static bool CanCreate(this PermissionValidator validator, Type resourceType)
        {
            return validator.CanPerformAction(resourceType, PermissionActions.Create);
        }

        /// <summary>
        /// Determines whether this instance can delete the specified resource identifier.
        /// </summary>
        /// <param name="validator">The validator.</param>
        /// <param name="resourceId">The resource identifier.</param>
        /// <returns></returns>
        public static bool CanDelete(this PermissionValidator validator, int resourceId)
        {
            return validator.CanPerformAction(resourceId, PermissionActions.Delete);
        }

        /// <summary>
        /// Determines whether this instance can delete the specified resource type.
        /// </summary>
        /// <param name="validator">The validator.</param>
        /// <param name="resourceType">Type of the resource.</param>
        /// <returns></returns>
        public static bool CanDelete(this PermissionValidator validator, Type resourceType)
        {
            return validator.CanPerformAction(resourceType, PermissionActions.Delete);
        }

        /// <summary>
        /// Determines whether this instance can list the specified resource identifier.
        /// </summary>
        /// <param name="validator">The validator.</param>
        /// <param name="resourceId">The resource identifier.</param>
        /// <returns></returns>
        public static bool CanList(this PermissionValidator validator, int resourceId)
        {
            return validator.CanPerformAction(resourceId, PermissionActions.List);
        }

        /// <summary>
        /// Determines whether this instance can list the specified resource type.
        /// </summary>
        /// <param name="validator">The validator.</param>
        /// <param name="resourceType">Type of the resource.</param>
        /// <returns></returns>
        public static bool CanList(this PermissionValidator validator, Type resourceType)
        {
            return validator.CanPerformAction(resourceType, PermissionActions.List);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    //Note: this class below can be replaced by an enum to make permission actions strongly typed
    public static class PermissionActions
    {
        public const int View = 1;
        public const int Edit = 2;
        public const int Create = 4;
        public const int Delete = 8;
        public const int List = 16;

        public static Dictionary<string, int> ActionList { get; } = new Dictionary<string, int>() { { "View", View },
            { "Edit", Edit }, { "Create", Create }, { "Delete", Delete }, { "List", List }};
    }

}
