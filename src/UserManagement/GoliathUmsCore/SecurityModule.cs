﻿using Autofac;
using Goliath.Security;
using Goliath.Ums.Services;
using Goliath.Web.Authorization;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;

namespace Goliath.Ums
{

    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Autofac.Module" />
    public class SecurityModule : Module
    {
        private readonly ApplicationSettingProvider settingProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="SecurityModule"/> class.
        /// </summary>
        /// <param name="settingProvider">The setting provider.</param>
        public SecurityModule(ApplicationSettingProvider settingProvider = null)
        {
            if (settingProvider == null)
                settingProvider = new ApplicationSettingProvider();

            this.settingProvider = settingProvider;
        }

        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<UniqueLongGenerator>().As<IUniqueNumberGenerator>();
            builder.RegisterType<RandomStringGenerator>().As<IRandomStringGenerator>();
            builder.RegisterType<SaltedSha256HashProvider>().As<IHashProvider>();
            builder.RegisterType<InMemoryPermissionStore>().As<IPermissionStore>();
            builder.RegisterType<ResourcePermissionGroupMapper>().As<IResourcePermissionGroupMapper>();
            builder.RegisterType<PermissionBuilder>().As<IPermissionBuilder>();
            builder.RegisterType<UserAccessManager>().As<IGoliathUserAccessManager>();
            builder.RegisterType<PermissionDataAdapter>().As<IPermissionDataAdapter>();


            var encryptionKey = settingProvider.GetAppSettingValue("encryptionKey");
            var hmacKey = settingProvider.GetAppSettingValue("hmacKey");

            builder.RegisterType<RijndaelSymmetricCryptoProvider>().As<ISymmetricCryptoProvider>()
                .WithParameter("encryptionKey", encryptionKey)
                .WithParameter("keySize", 256);

            builder.RegisterType<GoliathRijndaelProvider>().As<Nancy.Cryptography.IEncryptionProvider>();

            builder.RegisterType<HmacSha256Provider>().As<IHmacProvider>();
            builder.RegisterType<GoliathHmacProvider>().As<Nancy.Cryptography.IHmacProvider>()
                .WithParameter("length", 256)
                .WithParameter("secret", hmacKey);

            //OWIN data protection
            builder.RegisterType<GoliathDataProtector>().As<IDataProtector>();


            //asp .net and OWIN identity
            builder.RegisterType<GoliathDataProtectorProvider>().As<IDataProtectionProvider>();
            builder.RegisterType<IdentityUserStore>().As<IUserStore<UserSession, long>>();
            builder.RegisterType<GoliathUserManager>().As<UserManager<UserSession, long>>();
            builder.RegisterType<GoliathSignInManager>().As<SignInManager<UserSession, long>>();

            int ldapPortNumber;
            if (!int.TryParse(settingProvider.GetAppSettingValue("ldapPortNumber"), out ldapPortNumber))
                ldapPortNumber = 389;

            //LDAP
            builder.RegisterType<LdapAdapter>().As<ILdapAdapter>()
                .WithParameter("host", settingProvider.GetAppSettingValue("ldapServerHost"))
                .WithParameter("portNumber", ldapPortNumber)
                .WithParameter("rootDn", settingProvider.GetAppSettingValue("ldapRootDn"))
                .WithParameter("userContainerDn", settingProvider.GetAppSettingValue("ldapUserContainerDn"));
        }
    }
}
