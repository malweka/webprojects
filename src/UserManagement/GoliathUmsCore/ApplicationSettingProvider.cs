namespace Goliath.Ums
{
    public class ApplicationSettingProvider
    {
        public virtual string GetAppSettingValue(string keyName)
        {
            return System.Configuration.ConfigurationManager.AppSettings.Get(keyName);
        }
    }
}