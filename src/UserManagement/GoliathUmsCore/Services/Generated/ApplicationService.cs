//--------------------------------------------------------------------------------------------------
//<auto-generated>
//
//	This code was generated by a tool.
//	GoliathData Version: 1.4.1.18
//
//	Changes to this file may cause incorrect behavior and will be lost if
//	the code is regenerated. Please use a partial class to add functionalities.
//
//	Copyright Emmanuel Hameyie 2011-2017
//
//</auto-generated>
//--------------------------------------------------------------------------------------------------
// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable RedundantNameQualifier

using System;
using System.Collections.Generic;
using System.Linq;
using Goliath.Data;
using Goliath.Ums.Data;
using Goliath.Web;


namespace Goliath.Ums.Services
{
    /// <summary>
    /// CRUD Business service for entity Application.
    /// </summary>
    public partial class ApplicationService : BaseAppService, IApplicationService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationService"/> class.
        /// </summary>
        /// <param name="appContext">The application context.</param>
        public ApplicationService(ApplicationContext appContext) : base(appContext)
        {
            InitPartial();
        }

        //can be use to initialize any variables that may be needed. 
        partial void InitPartial();

        //apply extra filters in partial classes if needed.
        partial void ApplyExtraFilterPartial(Goliath.Data.Sql.IBinaryOperation<Application> query);
        partial void ApplyExtraFilterPartial(Goliath.Data.Sql.IQueryBuilder<Application> query);

        //after read done
        partial void OnReadOne(Application entity);
        partial void OnReadAll(IEnumerable<Application> list);
        partial void BeforeUpdate(Application entity);
        partial void BeforeDelete(Application entity);

        /// <summary>
        /// Gets the <see cref="Goliath.Ums.Data.Application"/> object by identifier
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <exception cref="SecurityException"></exception>
        public Application FetchOne(long id)
        {
            try
            {
                if (!CurrentContext.AuthorizatonValidator.CanView(typeof(Application)))
                    throw new SecurityException(string.Format("No sufficient permissions to perform *Read* action on entitity of type {0}.", typeof(Application)));

                using (var session = CurrentContext.SessionFactory.OpenSession())
                {
                    var dataAdapter = session.GetEntityDataAdapter<Application>();
                    var query = dataAdapter.Select().Where(c => c.Id).EqualToValue(id);
                    ApplyExtraFilterPartial(query);
                    var entity = query.FetchOne();

                    OnReadOne(entity);
                    return entity;
                }
            }
            catch (Exception ex)
            {
                throw new GoliathDataException(string.Format("Error while trying to read entity {0}", "Application"), ex);
            }
        }

        /// <summary>
        /// Get a list of all <see cref="Goliath.Ums.Data.Application"/> objects
        /// </summary>
        /// <param name="limit">The limit.</param>
        /// <param name="offset">The offset.</param>
        /// <param name="total">The total.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="sortString">The sort string.</param>
        /// <returns></returns>
        /// <exception cref="SecurityException"></exception>
        public ICollection<Application> FetchAll(int limit, int offset, out long total, string filter = null, string sortString = null)
        {
            try
            {
                if (!CurrentContext.AuthorizatonValidator.CanList(typeof(Application)))
                    throw new SecurityException(string.Format("No sufficient permissions to perform *List* action on entitity of type {0}.", typeof(Application)));

                using (var session = CurrentContext.SessionFactory.OpenSession())
                {
                    var query = session.SelectAll<Application>();
                    if (string.IsNullOrWhiteSpace(sortString))
                    {
                        //order by primary key because if we don't, the SQL Server query will fail.
                        sortString = PropertyNames.Application.Id;
                    }

                    ApplyExtraFilterPartial(query);
                    var list = FetchAllFromQuery(query, limit, offset, out total, filter, sortString);
                    OnReadAll(list);
                    return list;
                }
            }
            catch (Exception ex)
            {
                throw new GoliathDataException(string.Format("Error while trying to read entity {0}", "Application"), ex);
            }
        }

        /// <summary>
        /// Get a list of all <see cref="Goliath.Ums.Data.Application"/> objects
        /// </summary>
        /// <param name="limit">The limit.</param>
        /// <param name="offset">The offset.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="sortString">The sort string.</param>
        /// <returns></returns>
        /// <exception cref="SecurityException"></exception>
        public ICollection<Application> FetchAll(int limit, int offset, string filter = null, string sortString = null)
        {
            try
            {
                if (!CurrentContext.AuthorizatonValidator.CanList(typeof(Application)))
                    throw new SecurityException(string.Format("No sufficient permissions to perform *List* action on entitity of type {0}.", typeof(Application)));

                using (var session = CurrentContext.SessionFactory.OpenSession())
                {
                    var query = session.SelectAll<Application>();
                    if (string.IsNullOrWhiteSpace(sortString))
                    {
                        //order by primary key because if we don't, the SQL Server query will fail.
                        sortString = PropertyNames.Application.Id;
                    }

                    ApplyExtraFilterPartial(query);
                    var list = FetchAllFromQuery(query, limit, offset, filter, sortString);
                    OnReadAll(list);
                    return list;
                }
            }
            catch (Exception ex)
            {
                throw new GoliathDataException(string.Format("Error while trying to read entity {0}", "Application"), ex);
            }
        }

        /// <summary>
        /// Get a list of all <see cref="Goliath.Ums.Data.Application"/> objects
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <param name="sortString">The sort string.</param>
        /// <returns></returns>
        /// <exception cref="SecurityException"></exception>
        public ICollection<Application> FetchAll(string filter = null, string sortString = null)
        {
            try
            {
                if (!CurrentContext.AuthorizatonValidator.CanList(typeof(Application)))
                    throw new SecurityException(string.Format("No sufficient permissions to perform *List* action on entitity of type {0}.", typeof(Application)));

                using (var session = CurrentContext.SessionFactory.OpenSession())
                {
                    var query = session.SelectAll<Application>();
                    ApplyExtraFilterPartial(query);
                    var list = FetchAllFromQuery(query, filter, sortString);
                    OnReadAll(list);
                    return list;
                }
            }
            catch (Exception ex)
            {
                throw new GoliathDataException(string.Format("Error while trying to read entity {0}", "Application"), ex);
            }
        }


        /// <summary>
        /// Create and save entity of type <see cref="Goliath.Ums.Data.Application"/> objects
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="SecurityException"></exception>
        public void Create(Application entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException(nameof(entity));

                if (!CurrentContext.AuthorizatonValidator.CanCreate(typeof(Application)))
                    throw new SecurityException(string.Format("No sufficient permissions to perform *Create* action on entity of type {0}.", typeof(Application)));

                using (var session = CurrentContext.SessionFactory.OpenSession())
                {
                    SetCreateable(entity);
                    session.Insert(entity);
                }
            }
            catch (Exception ex)
            {
                throw new GoliathDataException(string.Format("Error while trying to create entity {0}", "Application"), ex);
            }
        }

        /// <summary>
        /// Update and save entity of type <see cref="Goliath.Ums.Data.Application"/> objects
        /// </summary>
        /// <param name="updateEntity">The update entity.</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="SecurityException"></exception>
        public void Update(Application updateEntity)
        {
            try
            {
                if (updateEntity == null)
                    throw new ArgumentNullException(nameof(updateEntity));

                if (!CurrentContext.AuthorizatonValidator.CanEdit(typeof(Application)))
                    throw new SecurityException(string.Format("No sufficient permissions to perform *Edit* action on entity of type {0}.", typeof(Application)));

                using (var session = CurrentContext.SessionFactory.OpenSession())
                {
                    var dataAdapter = session.GetEntityDataAdapter<Application>();
                    var query = dataAdapter.Select().Where(c => c.Id).EqualToValue(updateEntity.Id);
                    ApplyExtraFilterPartial(query);
                    var entity = query.FetchOne();

                    if (entity == null)
                        throw new GoliathEntityNotFoundException("entity not found");

                    BeforeUpdate(updateEntity);
                    entity.Name = updateEntity.Name;
                    entity.AppKey = updateEntity.AppKey;
                    entity.Url = updateEntity.Url;
                    entity.LogoutUrl = updateEntity.LogoutUrl;
                    entity.ModifiedOn = updateEntity.ModifiedOn;
                    entity.ModifiedBy = updateEntity.ModifiedBy;

                    if (!entity.IsDirty) return;
                    SetModifiable(entity);
                    session.Update(entity);
                }
            }
            catch (Exception ex)
            {
                throw new GoliathDataException(string.Format("Error while trying to edit entity {0}", "Application"), ex);
            }
        }

        /// <summary>
        /// Delete entity of type <see cref="Goliath.Ums.Data.Application"/> objects
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <exception cref="SecurityException"></exception>
        public void Delete(long id)
        {
            try
            {
                if (!CurrentContext.AuthorizatonValidator.CanDelete(typeof(Application)))
                    throw new SecurityException(string.Format("No sufficient permissions to perform *Delete* action on entitity of type {0}.", typeof(Application)));

                using (var session = CurrentContext.SessionFactory.OpenSession())
                {
                    var dataAdapter = session.GetEntityDataAdapter<Application>();
                    dataAdapter.DeleteById(id);
                }
            }
            catch (Exception ex)
            {
                throw new GoliathDataException(string.Format("Error while trying to delete entity {0}", "Application"), ex);
            }
        }
    }
}