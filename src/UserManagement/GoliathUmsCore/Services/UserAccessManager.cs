using System;
using System.Linq;
using Goliath.Data;
using Goliath.Models;
using Goliath.Security;
using Goliath.Ums.Data;
using Goliath.Web.Authorization;
using DataExtensions = Goliath.Ums.Data.DataExtensions;

namespace Goliath.Ums.Services
{
    public class UserAccessManager : IGoliathUserAccessManager
    {
        readonly IDatabaseProvider dbProvider;
        readonly IHashProvider hashProvider;
        readonly ICacheProvider cache;
        readonly ILdapAdapter ldapAdapter;

        public UserAccessManager(IDatabaseProvider dbProvider, IHashProvider hashProvider, ICacheProvider cache, ILdapAdapter ldapAdapter)
        {
            if (dbProvider == null) throw new ArgumentNullException(nameof(dbProvider));
            if (hashProvider == null) throw new ArgumentNullException(nameof(hashProvider));
            if (cache == null) throw new ArgumentNullException(nameof(cache));
            if (ldapAdapter == null) throw new ArgumentNullException(nameof(ldapAdapter));

            this.dbProvider = dbProvider;
            this.hashProvider = hashProvider;
            this.cache = cache;
            this.ldapAdapter = ldapAdapter;
        }


        public AuthResult AuthenticateExternalUser(string provider, string externalUserId, string ipAddress)
        {
            if (provider == null) throw new ArgumentNullException(nameof(provider));

            var user = GetUser(provider, externalUserId);
            AuthResult result = new AuthResult();

            using (var session = dbProvider.SessionFactory.OpenSession())
            {
                session.BeginTransaction();
                var roleAdapter = session.GetEntityDataAdapter<Role>();
                try
                {
                    UserAccessLog accessLog;
                    var loginDate = DateTime.UtcNow;
                    if (user == null)
                    {
                        accessLog = new UserAccessLog() { ActivityType = ActivityLogType.UnknowUser, UserName = externalUserId, CreatedOn = loginDate, IpAddress = ipAddress };
                        session.Insert(accessLog);
                        result.Status = AuthenticationStatus.Failed;
                    }
                    else
                    {
                        accessLog = new UserAccessLog() { UserName = user.UserName, CreatedOn = loginDate, IpAddress = ipAddress };
                        result.Status = AuthenticationStatus.Success;

                        var roles = roleAdapter.GetUserRoles(user.Id).ToDictionary<Role, string, IRole>(appRole => appRole.Name, appRole => appRole);
                        result.UserSession = DataExtensions.CreateUserSession(user, roles);
                        result.UserSession.Roles = roles;

                        result.UserSession.AuthenticationProvider = provider;
                        user.LastActivityOn = loginDate;

                        var activityType = ActivityLogType.LoginSuccess;

                        if (!user.IsActive)
                        {
                            activityType = ActivityLogType.InactiveUser;
                            result.Status = result.Status | AuthenticationStatus.Disabled;
                        }

                        accessLog.ActivityType = activityType;
                        session.Insert(accessLog);
                        session.Update(user);
                    }

                    session.CommitTransaction();
                    return result;
                }
                catch (Exception exception)
                {
                    session.RollbackTransaction();
                    throw new SecurityException("Error trying to authenticate User", exception);
                }
            }
        }

        public AuthResult AuthenticateLocalUser(string userName, string password, string ipAddress)
        {
            var result = new AuthResult() { Status = AuthenticationStatus.Failed };
            var loginDate = DateTime.UtcNow;

            if (string.IsNullOrWhiteSpace(userName) || string.IsNullOrWhiteSpace(password))
            {
                result.Status = AuthenticationStatus.Failed;
                return result;
            }

            using (var session = dbProvider.SessionFactory.OpenSession())
            {
                session.BeginTransaction();
                var roleAdapter = session.GetEntityDataAdapter<Role>();
                try
                {
                    UserAccessLog accessLog;
                    var user = GetUser(userName);

                    if (user != null)
                    {

                        var isValid = AuthenticateInternal(user, password);

                        if (isValid)
                        {
                            result.Status = AuthenticationStatus.Success;
                            var roles = roleAdapter.GetUserRoles(user.Id).ToDictionary<Role, string, IRole>(appRole => appRole.Name, appRole => appRole);

                            result.UserSession = DataExtensions.CreateUserSession(user, roles);
                            user.LastActivityOn = loginDate;

                            accessLog = new UserAccessLog() { ActivityType = ActivityLogType.LoginSuccess, UserName = userName, CreatedOn = loginDate, IpAddress = ipAddress };

                            if (!user.IsActive)
                            {
                                result.Status = result.Status | AuthenticationStatus.Disabled;
                                accessLog.ActivityType = ActivityLogType.InactiveUser;
                            }

                            session.Insert(accessLog);
                            session.Update(user);
                        }
                        else
                        {
                            accessLog = new UserAccessLog() { ActivityType = ActivityLogType.InvalidPassword, UserName = userName, CreatedOn = loginDate, IpAddress = ipAddress };
                            session.Insert(accessLog);
                        }
                    }
                    else
                    {
                        accessLog = new UserAccessLog() { ActivityType = ActivityLogType.UnknowUser, UserName = userName, CreatedOn = loginDate, IpAddress = ipAddress };
                        session.Insert(accessLog);
                        result.Status = AuthenticationStatus.Failed;
                    }

                    session.CommitTransaction();
                    return result;
                }
                catch (Exception exception)
                {
                    session.RollbackTransaction();
                    throw new SecurityException("Error trying to authenticate User", exception);
                }
            }
        }

        bool AuthenticateInternal(User user, string password)
        {
            //if (user. && !string.IsNullOrWhiteSpace(user.ExternalUserId))
            //    return AuthenticateLdapUser(user, password);

            var pwd = user.PasswordHash.Replace("{SSHA256.b64}", string.Empty);
            return hashProvider.VerifyHash(password.ConvertToByteArray(), pwd.ConvertFromBase64StringToByteArray());
        }

        bool AuthenticateLdapUser(User user, string password)
        {
            return ldapAdapter.Authenticate(user.ExternalUserId, password);
        }

        public UserSession FindUser(long userId)
        {
            var user = GetUser(userId);

            using (var session = dbProvider.SessionFactory.OpenSession())
            {
                var roleAdapter = session.GetEntityDataAdapter<Role>();
                var roles = roleAdapter.GetUserRoles(user.Id).ToDictionary<Role, string, IRole>(appRole => appRole.Name, appRole => appRole);
                return DataExtensions.CreateUserSession(user, roles);
            }
        }

        public UserSession FindUser(string userName)
        {
            var user = GetUser(userName);

            using (var session = dbProvider.SessionFactory.OpenSession())
            {
                var roleAdapter = session.GetEntityDataAdapter<Role>();
                var roles = roleAdapter.GetUserRoles(user.Id).ToDictionary<Role, string, IRole>(appRole => appRole.Name, appRole => appRole);
                return DataExtensions.CreateUserSession(user, roles);
            }
        }

        static string GetCacheKey(long userId)
        {
            return UserSession.CacheKeyName + userId;
        }

        public UserSession GetUserFromCache(long userId)
        {
            var cacheKey = GetCacheKey(userId);
            var user = cache.Get<UserSession>(cacheKey);

            if (user == null)
            {
                //user is not in cache. Let's retrieve from database and cache.
                user = FindUser(userId);
                if (user == null)
                    return null;

                cache.Set(cacheKey, user);
            }

            return user;
        }

        public void CacheUser(UserSession sessionUser)
        {
            if (sessionUser == null)
                throw new ArgumentNullException(nameof(sessionUser));

            cache.Set(GetCacheKey(sessionUser.Id), sessionUser);
        }

        public void CacheForgetUser(long userId)
        {
            var cacheKey = GetCacheKey(userId);
            cache.Remove(cacheKey);
        }

        User GetUser(string provider, string externalUserId)
        {
            using (var session = dbProvider.SessionFactory.OpenSession())
            {
                var activeUser = session.SelectAll<User>()
                    .Where(c => c.AuthenticationProvider)
                    .EqualToValue(provider)
                    .And(c => c.ExternalScreenName)
                    .EqualToValue(externalUserId).FetchOne();

                return activeUser;
            }
        }

        User GetUser(string userName)
        {
            using (var session = dbProvider.SessionFactory.OpenSession())
            {
                var adapter = session.GetEntityDataAdapter<User>();
                var activeUser = adapter.GetByUserName(userName);

                return activeUser;
            }
        }

        User GetUser(long id)
        {
            using (var session = dbProvider.SessionFactory.OpenSession())
            {
                var adapter = session.GetEntityDataAdapter<User>();
                var activeUser = adapter.GetById(id);

                return activeUser;
            }
        }
    }
}