﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Goliath.Ums.Models;
using Goliath.Web.Authorization;
using Novell.Directory.Ldap;

namespace Goliath.Ums.Services
{
    public class LdapAdapter : ILdapAdapter
    {
        public LdapAdapter(string host, int portNumber, string rootDn, string userContainerDn)
        {
            this.Host = host;
            this.PortNumber = portNumber;
            this.RootDn = rootDn;
            this.UserContainerDn = userContainerDn;
            if (string.IsNullOrWhiteSpace(userContainerDn))
                this.UserContainerDn = rootDn;
        }

        public LdapAdapter(string host, string rootDn, string userContainerDn) : this(host, 389, rootDn, userContainerDn)
        {

        }

        public string RootDn { get; }

        public string UserContainerDn { get; }

        public string Host { get; }

        public int PortNumber { get; }

        /// <summary>
        /// Authenticates the specified user.
        /// </summary>
        /// <param name="userDn">The user distinguish name.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        public bool Authenticate(string userDn, string password)
        {
            try
            {
                ConnectToLdapServer(userDn, password);

                //if no exception was thrown than the user was valid.
                return true;
            }
            catch
            {
                return false;
            }
        }

        LdapConnection ConnectToLdapServer(string userDn, string password)
        {
            var ldapConnection = new LdapConnection();
            ldapConnection.Connect(Host, PortNumber);
            ldapConnection.Bind(userDn, password);
            return ldapConnection;
        }

        /// <summary>
        /// Finds all users inside the container that match the filter.
        /// </summary>
        /// <param name="userDn">The user distinguish name.</param>
        /// <param name="password">The password.</param>
        /// <param name="containerDn">The container distinguish name.</param>
        /// <param name="filter">The filter.</param>
        /// <returns></returns>
        public IList<UserSession> FindAllUsers(string userDn, string password, string containerDn, string filter)
        {
            var foundUsers = new List<UserSession>();
            var ldapConnection = ConnectToLdapServer(userDn, password);
            var result = ldapConnection.Search(containerDn, LdapConnection.SCOPE_SUB, filter, null, false);
            while (result.hasMore())
            {
                var entry = result.next();
                var user = CreateUserFromLdapEntry(entry);
                if (user != null)
                {
                    foundUsers.Add(user);
                }
            }
            return foundUsers;
        }

        /// <summary>
        /// Finds all groups.
        /// </summary>
        /// <param name="userDn">The user distinguish name.</param>
        /// <param name="password">The password.</param>
        /// <param name="containerDn">The root distinguish name.</param>
        /// <param name="filter">The filter.</param>
        /// <returns></returns>
        public IList<LdapGroupModel> FindAllGroups(string userDn, string password, string containerDn, string filter)
        {
            var groups = new List<LdapGroupModel>();
            var ldapConnection = ConnectToLdapServer(userDn, password);
            var result = ldapConnection.Search(containerDn, LdapConnection.SCOPE_SUB, filter, null, false);

            while (result.hasMore())
            {
                var entry = result.next();
                var group = new LdapGroupModel(); ;
                var name = entry.getAttribute("name") ?? entry.getAttribute("cn");
                group.Name = name?.StringValue;
                group.DisplayName = entry.getAttribute("displayName")?.StringValue;
                group.DistinguishedName = entry.getAttribute("distinguishedName")?.StringValue;

                groups.Add(group);
            }

            return groups;
        }

        /// <summary>
        /// Finds all members of group.
        /// </summary>
        /// <param name="userDn">The user distinguish name.</param>
        /// <param name="password">The password.</param>
        /// <param name="containerDn"></param>
        /// <param name="groupName">The group name.</param>
        /// <returns></returns>
        public IList<UserSession> FindAllMembersOfGroup(string userDn, string password, string containerDn, string groupName)
        {
            var foundUsers = new List<UserSession>();
            var ldapConnection = ConnectToLdapServer(userDn, password);
            var result = ldapConnection.Search(containerDn, LdapConnection.SCOPE_SUB, string.Concat("name=", groupName), null, false);
            while (result.hasMore())
            {
                var entry = result.next();
                var members = entry.getAttribute("member");

                if (members?.StringValueArray == null) return new List<UserSession>();

                foreach (var memberDn in members.StringValueArray)
                {
                    var lsearch = ldapConnection.Search(UserContainerDn, LdapConnection.SCOPE_SUB, string.Concat("distinguishedName=", memberDn), null, false);
                    if (lsearch.hasMore())
                    {
                        var uentry = lsearch.next();
                        var user = CreateUserFromLdapEntry(uentry);
                        foundUsers.Add(user);
                    }
                }
            }
            return foundUsers;
        }

        UserSession CreateUserFromLdapEntry(LdapEntry entry)
        {
            var userName = entry.getAttribute("userPrincipalName") ?? entry.getAttribute("name");
            if (userName == null)
                return null;

            var usr = new UserSession { UserName = userName.StringValue };
            usr.FirstName = entry.getAttribute("givenName")?.StringValue;
            usr.LastName = entry.getAttribute("sn")?.StringValue;
            usr.EmailAddress = entry.getAttribute("mail")?.StringValue;
            usr.ExternalUserId = entry.getAttribute("distinguishedName")?.StringValue;

            return usr;
        }
    }
}
