﻿using System.Collections.Generic;
using Goliath.Security;
using Goliath.Web.Authorization;

namespace Goliath.Ums.Data
{
    public static class DataExtensions
    {
        /// <summary>
        /// Creates the user session.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="roles">The roles.</param>
        /// <returns></returns>
        public static UserSession CreateUserSession(this Ums.Data.User user, IDictionary<string, IRole> roles = null)
        {
            if (user == null) return null;

            if (roles == null)
                roles = new Dictionary<string, IRole>();

            var session = new UserSession
            {
                Id = user.Id,
                UserName = user.UserName,
                IsActive = user.IsActive,
                Roles = roles,
                FirstName = user.FirstName,
                LastName = user.LastName,
                EmailAddress = user.EmailAddress,
            };

            var stringGenerator = new RandomStringGenerator();
            session.SessionId = stringGenerator.Generate(24, false);

            return session;
        }

    }
}