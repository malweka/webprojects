﻿using System;

namespace Goliath.Ums.Data
{
    public enum PostStatus
    {
        Draft = 0,
        Published = 1,
        NeedReview = 2,
        InReview = 3,
        ReadyForPublishing = 4,
        Expired = 5,
    }

    public enum ActivityLogType
    {
        UnknowUser = 0,
        InactiveUser,
        InvalidPassword,
        LoginSuccess,
        EmailChange,
        PasswordReset,
    }

    public enum TokenType
    {
        PasswordReset = 0,
        UserCreation = 1,
        VerifyEmail,
        VerifyCellPhone,
    }

    [Flags]
    public enum PermActionType
    {
        None = 0,
        View = 1,
        Edit = 2,
        Create = 4,
        Delete = 8,
        List = 16
    }

}
