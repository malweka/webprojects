﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Goliath.Ums.Data
{
    public partial class User
    {
        /// <summary>
        /// Gets a value indicating whether this instance is internal user.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is internal user; otherwise, <c>false</c>.
        /// </value>
        public bool IsInternalUser
        {
            get { return !string.IsNullOrWhiteSpace(PasswordHash); }
        }
    }
}
