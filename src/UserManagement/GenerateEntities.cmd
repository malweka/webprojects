GoliathData -generateEntities -namespace="Goliath.Ums.Data"^
 -templateFolder="%~dp0Templates"^
 -workingFolder="%~dp0GoliathUmsCore\Data\Generated"^
 -m="%~dp0GoliathUmsHosting\data.map.config"
 
GoliathData -generate -in="EntityResource.razt" -out="EntityResources.resx" -namespace="Goliath.Ums.Data"^
 -templateFolder="%~dp0Templates"^
 -workingFolder="%~dp0GoliathUmsCore\Data\Generated"^
 -m="%~dp0GoliathUmsHosting\data.map.config"
 
GoliathData -generate -in="DataAccessExtensions.razt"^
 -out="DataAccessExtensions.cs" -namespace="Goliath.Ums.Data"^
 -templateFolder="%~dp0Templates"^
 -workingFolder="%~dp0GoliathUmsCore\Data\Generated"^
 -m="%~dp0GoliathUmsHosting\data.map.config"
 
GoliathData -generateAll -in="EntityValidator.razt" -out="Validator.cs" -namespace="Goliath.Ums.Data"^
 -templateFolder="%~dp0Templates"^
 -workingFolder="%~dp0GoliathUmsCore\Data\Generated"^
 -m="%~dp0GoliathUmsHosting\data.map.config" 
 
 GoliathData -generateAll -in="Dto.razt" -out="(name).cs" -namespace="Goliath.Ums.Dtos"^
 -templateFolder="%~dp0Templates"^
 -workingFolder="%~dp0GoliathUmsCore\Dtos\Generated"^
 -m="%~dp0GoliathUmsHosting\data.map.config"
 
pause
