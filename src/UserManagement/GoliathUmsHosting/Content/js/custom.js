$(document).ready(function () {
    $('.navbar-nav [data-toggle="tooltip"]').tooltip();
    $('.navbar-twitch-toggle').on('click', function (event) {
        event.preventDefault();
        $('.navbar').toggleClass('open');
        var width = $('.navbar-twitch').width() - 70;
        var widthHeader = $('.navbar-twitch').width() - 86;
        console.log(width);
        $('.content').css({
            marginLeft: width
        });

        $('.navbar-primary .navbar .navbar-header .navbar-brand').css({
            marginLeft: widthHeader
        });

    });

    $('.nav-style-toggle').on('click', function (event) {
        event.preventDefault();
        var $current = $('.nav-style-toggle.disabled');
        $(this).addClass('disabled');
        $current.removeClass('disabled');
        $('.navbar-twitch').removeClass('navbar-' + $current.data('type'));
        $('.navbar-twitch').addClass('navbar-' + $(this).data('type'));
    });

    resizeBody();
});

$(window).resize(function() {
    resizeBody();
});

function resizeBody() {
    var wHeight = $(window).height();
    var contentHeight = $("#mainContent").height();
    var newHeight = wHeight - 126;
    if (contentHeight < newHeight) {
        $("#mainContent").height(newHeight);
    }
    
}