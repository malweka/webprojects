﻿using System;
using System.Linq;
using Goliath.Models;
using Goliath.Ums.Data;
using Goliath.Ums.Services;
using Nancy;

namespace Goliath.Ums.Modules
{
    public class UserModule : SecureModule
    {
        private readonly IUserService userService;

        public UserModule(IUserService userService):base("user")
        {
            this.userService = userService;
            Get["/modal/"] = ListUsersModal;
        }

        dynamic ListUsersModal(dynamic parameters)
        {

            long total;
            var users = userService.FetchAll(ApiBaseModule.Limit, 0, out total, null, PropertyNames.User.LastName);
            var model = new ListViewModel<User>() { TabName = "Access", Title = "User Access Control", Items = users.ToList()};
            model.TotalPages = (int)Math.Ceiling((double)total / ApiBaseModule.Limit);
            model.CurrentPage = 1;
           

            return Negotiate.WithModel(model).WithView("modal_list");
        }
    }
}
