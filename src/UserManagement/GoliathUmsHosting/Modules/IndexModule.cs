﻿using Goliath.Models;
using Nancy.Security;

namespace Goliath.Ums.Modules
{
    public class IndexModule : SecureModule
    {
        const string TabName = "Dashboard";

        public IndexModule()
        {
            Get["/"] = parameters =>
            {
                return View["index", new DisplayViewModel() { TabName = TabName, Title = "Dashboard"}];
            };

            Get["/acct"] = parameters =>
            {
                var user = Context.GetMSOwinUser();

                this.ViewBag.Title = "Account";
                return View["acct"];
            };

            Get["/sauth/{v}"] = parameters =>
            {
                 
                return View["acct"];
            };
        }
    }
}
