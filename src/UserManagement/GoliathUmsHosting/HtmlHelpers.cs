using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Goliath.Models;
using Goliath.Ums.Data;
using Goliath.Web;
using Goliath.Web.Authorization;
using Nancy.ViewEngines.Razor;

namespace Goliath.Ums
{
    public static class HtmlHelpers
    {
        /// <summary>
        /// Determines whether the current user can view the specified resource type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="resourceType">Type of the resource.</param>
        /// <returns></returns>
        public static bool CanView<T>(this HtmlHelpers<T> htmlHelper, int resourceType)
        {
            return htmlHelper.CanPerformAction(resourceType, PermissionActions.View);
        }

        /// <summary>
        /// Determines whether the current user can view the specified resource type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="resourceType">Type of the resource.</param>
        /// <returns></returns>
        public static bool CanView<T>(this HtmlHelpers<T> htmlHelper, Type resourceType)
        {
            return htmlHelper.CanPerformAction(resourceType, PermissionActions.View);
        }

        /// <summary>
        /// Determines whether the current user can edit the specified resource type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="resourceType">Type of the resource.</param>
        /// <returns></returns>
        public static bool CanEdit<T>(this HtmlHelpers<T> htmlHelper, int resourceType)
        {
            return htmlHelper.CanPerformAction(resourceType, PermissionActions.Edit);
        }

        /// <summary>
        /// Determines whether the current user can edit the specified resource type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="resourceType">Type of the resource.</param>
        /// <returns></returns>
        public static bool CanEdit<T>(this HtmlHelpers<T> htmlHelper, Type resourceType)
        {
            return htmlHelper.CanPerformAction(resourceType, PermissionActions.Edit);
        }

        /// <summary>
        /// Determines whether the current user can create the specified resource type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="resourceType">Type of the resource.</param>
        /// <returns></returns>
        public static bool CanCreate<T>(this HtmlHelpers<T> htmlHelper, int resourceType)
        {
            return htmlHelper.CanPerformAction(resourceType, PermissionActions.Create);
        }

        /// <summary>
        /// Determines whether the current user can create the specified resource type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="resourceType">Type of the resource.</param>
        /// <returns></returns>
        public static bool CanCreate<T>(this HtmlHelpers<T> htmlHelper, Type resourceType)
        {
            return htmlHelper.CanPerformAction(resourceType, PermissionActions.Create);
        }

        /// <summary>
        /// Determines whether the current user can delete the specified resource type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="resourceType">Type of the resource.</param>
        /// <returns></returns>
        public static bool CanDelete<T>(this HtmlHelpers<T> htmlHelper, int resourceType)
        {
            return htmlHelper.CanPerformAction(resourceType, PermissionActions.Delete);
        }

        /// <summary>
        /// Determines whether the current user can delete the specified resource type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="resourceType">Type of the resource.</param>
        /// <returns></returns>
        public static bool CanDelete<T>(this HtmlHelpers<T> htmlHelper, Type resourceType)
        {
            return htmlHelper.CanPerformAction(resourceType, PermissionActions.Delete);
        }

        /// <summary>
        /// Determines whether the current user can list the specified resource type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="resourceType">Type of the resource.</param>
        /// <returns></returns>
        public static bool CanList<T>(this HtmlHelpers<T> htmlHelper, int resourceType)
        {
            return htmlHelper.CanPerformAction(resourceType, PermissionActions.List);
        }

        /// <summary>
        /// Determines whether the current user can list the specified resource type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="htmlHelper">The HTML helper.</param>
        /// <param name="resourceType">Type of the resource.</param>
        /// <returns></returns>
        public static bool CanList<T>(this HtmlHelpers<T> htmlHelper, Type resourceType)
        {
            return htmlHelper.CanPerformAction(resourceType, PermissionActions.List);
        }

        /// <summary>
        /// Get localized text from  EntityResource
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="helpers">The helpers.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static string TextFromEntityResource<T>(this HtmlHelpers<T> helpers, string name)
        {
            var currentCulture = helpers.RenderContext.Context.Culture ?? new CultureInfo("en");
            return EntityResources.ResourceManager.GetString(name, currentCulture);
        }

        /// <summary>
        /// Gets the current user.
        /// </summary>
        /// <param name="helpers">The helpers.</param>
        /// <returns></returns>
        public static UserSession GetCurrentUser(this Nancy.ViewEngines.Razor.HtmlHelpers helpers)
        {
            UserSession user = null;
            if (helpers.IsAuthenticated)
            {
                user = helpers.CurrentUser as UserSession;
            }

            return user;
        }

        public static IHtmlString PrintActivityType(this Nancy.ViewEngines.Razor.HtmlHelpers helpers, ActivityLogType activityType)
        {
            string displayText;
            switch (activityType)
            {
                case ActivityLogType.UnknowUser:
                    displayText = string.Concat("<i class=\"fa fa-user-secret\"></i> ", AppStringResources.LabelUnknownUser);
                    break;
                case ActivityLogType.InactiveUser:
                    displayText = string.Concat("<i class=\"fa fa-exclamation-circle\"></i> ", AppStringResources.LabelInactiveUser);
                    break;
                case ActivityLogType.InvalidPassword:
                    displayText = string.Concat("<i class=\"fa fa-user-times\"></i> ", AppStringResources.LabelInvalidPassword);
                    break;
                case ActivityLogType.LoginSuccess:
                    displayText = string.Concat("<i class=\"fa fa-check\"></i> ", AppStringResources.LabelLoginSuccess);
                    break;
                case ActivityLogType.EmailChange:
                    displayText = string.Concat("<i class=\"fa fa-envelope-o\"></i> ", AppStringResources.LabelEmailChange);
                    break;
                case ActivityLogType.PasswordReset:
                    displayText = string.Concat("<i class=\"fa fa-refresh\"></i> ", AppStringResources.LabelPasswordReset);
                    break;
                default:
                    //displayText = activityType.ToString();
                    displayText = string.Concat("<i></i> ", activityType.ToString());
                    break;
            }
            return new NonEncodedHtmlString(displayText);
        }

        public static IHtmlString PermissionList(this Nancy.ViewEngines.Razor.HtmlHelpers helpers, PermissionActionModel permission, Dictionary<string,int> actions, int iteration)
        {
            StringBuilder html = new StringBuilder("<div class=\"\">");
            foreach (var act in actions)
            {
                string ischecked = string.Empty;
                var name = $"PermissionActions_{iteration}[{act.Key}]";
                if ((permission.PermValue & act.Value) == act.Value)
                    ischecked = "checked";
               // html.AppendFormat("<input type=\"hidden\" name=\"{0}.Key\" value=\"{1}\"/>", name,act.Key);
                html.AppendFormat("<label class=\"checkbox-inline\"><input type=\"checkbox\" {0} name=\"{1}\" value=\"{2}\">{3}</label>\n\r", ischecked, name, act.Value, act.Key);
            }
            html.AppendLine("</div>");

            return new NonEncodedHtmlString(html.ToString());
        }
    }
}