﻿using Autofac;
using Goliath.Data;
using Goliath.Web;
using Goliath.Web.Authorization;
using Microsoft.AspNet.Identity;
using Nancy;
using Nancy.Bootstrappers.Autofac;
using Nancy.Security;

namespace Goliath.Ums
{
    /// <summary>
    /// 
    /// </summary>
    public class Bootstrapper : AutofacNancyBootstrapper
    {

        protected override IRootPathProvider RootPathProvider => new CustomRootPathProvider();

        private readonly ILifetimeScope lifetimeScope;

        /// <summary>
        /// Initializes a new instance of the <see cref="Bootstrapper" /> class.
        /// </summary>
        /// <param name="lifetimeScope">The lifetime scope.</param>
        public Bootstrapper(ILifetimeScope lifetimeScope)
        {
            this.lifetimeScope = lifetimeScope; 
        }

        /// <summary>
        /// Gets the application container.
        /// </summary>
        /// <returns></returns>
        protected override ILifetimeScope GetApplicationContainer()
        {
            return lifetimeScope;
        }

        /// <summary>
        /// Configures the request container.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="context">The context.</param>
        protected override void ConfigureRequestContainer(ILifetimeScope container, NancyContext context)
        {
            base.ConfigureRequestContainer(container, context);

            //let's get the current user if logged in. 
            if (!context.Items.ContainsKey("OWIN_REQUEST_ENVIRONMENT")) return;

            var builder = new ContainerBuilder();
            var owinUser = context.GetMSOwinUser();

            if (owinUser == null) return; //if there's no Owin User then the user is not logged in.

            var identity = owinUser.Identity;
            var permServ = container.Resolve<IPermissionBuilder>();
            var dbProvider = container.Resolve<IDatabaseProvider>();
            var accessManager = container.Resolve<IGoliathUserAccessManager>();

            var userId = identity.GetUserId<long>();
            var userSession = accessManager.GetUserFromCache(userId);
            userSession.PermissionService = permServ;
            var perRequestAppContext = new PerRequestContext(dbProvider, permServ, userSession);

            builder.RegisterInstance(perRequestAppContext).As<ApplicationContext>();
            builder.Update(container.ComponentRegistry);

            context.CurrentUser = userSession;
        }

        /// <summary>
        /// Gets the cryptography configuration.
        /// </summary>
        /// <value>
        /// The cryptography configuration.
        /// </value>
        protected override Nancy.Cryptography.CryptographyConfiguration CryptographyConfiguration
        {
            get
            {
                var cryptoProvider = lifetimeScope.Resolve<Nancy.Cryptography.IEncryptionProvider>();
                var hmac = lifetimeScope.Resolve<Nancy.Cryptography.IHmacProvider>();

                return new Nancy.Cryptography.CryptographyConfiguration(cryptoProvider, hmac);
            }
        }
    }
}
