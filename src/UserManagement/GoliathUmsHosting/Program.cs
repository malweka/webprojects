﻿#if DEBUG
using ServiceHost.Debugger;

#else
using System.ServiceProcess;
#endif

namespace Goliath.Ums
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            var servicesToRun = new ServiceBase[]
            {
                new CmsServiceHost()
            };

            ServiceBase.Run(servicesToRun);
        }
    }
}
