GoliathData -generate -in="ResourceTypeMap.razt" -out="ResourceEntityMap.cs" -namespace="Goliath.Ums.Services"^
 -templateFolder="%~dp0Templates"^
 -workingFolder="%~dp0GoliathUmsCore\Services\Generated"^
 -m="%~dp0GoliathUmsHosting\data.map.config"
 
 GoliathData -generateAll -in="CrudServiceInterface.razt" -out="I(name)Service.cs" -namespace="Goliath.Ums.Services"^
 -templateFolder="%~dp0Templates"^
 -workingFolder="%~dp0GoliathUmsCore\Services\Generated"^
 -m="%~dp0GoliathUmsHosting\data.map.config"
 
  GoliathData -generateAll -in="CrudServiceImpl.razt" -out="(name)Service.cs" -namespace="Goliath.Ums.Services"^
 -templateFolder="%~dp0Templates"^
 -workingFolder="%~dp0GoliathUmsCore\Services\Generated"^
 -m="%~dp0GoliathUmsHosting\data.map.config"
 
 GoliathData -generate -in="DataServiceExtensions.razt"^
 -out="DataServiceExtensions.cs" -namespace="Goliath.Ums.Services"^
 -templateFolder="%~dp0Templates"^
 -workingFolder="%~dp0GoliathUmsCore\Services\Generated"^
 -m="%~dp0GoliathUmsHosting\data.map.config"
 
pause
