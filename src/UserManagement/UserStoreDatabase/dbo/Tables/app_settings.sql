﻿CREATE TABLE [dbo].[app_settings] (
    [id]            BIGINT          NOT NULL,
    [key_name]      VARCHAR (50)    NOT NULL,
    [visual_editor] VARCHAR (50)    CONSTRAINT [DF_app_settings_visual_editor] DEFAULT ('TextBox') NOT NULL,
    [key_value]     NVARCHAR (4000) NULL,
    [created_on]    DATETIME        CONSTRAINT [DF_app_settings_created_on] DEFAULT (getutcdate()) NOT NULL,
    [updated_on]    DATETIME        NULL,
    CONSTRAINT [PK_app_config_settings] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [IX_app_config_settings] UNIQUE NONCLUSTERED ([key_name] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'application configuration settings', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'app_settings';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identifier field', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'app_settings', @level2type = N'COLUMN', @level2name = N'id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Config key name: key name must be unique.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'app_settings', @level2type = N'COLUMN', @level2name = N'key_name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Config key value', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'app_settings', @level2type = N'COLUMN', @level2name = N'key_value';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date and time when setting was created.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'app_settings', @level2type = N'COLUMN', @level2name = N'created_on';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Date and time setting was last modified.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'app_settings', @level2type = N'COLUMN', @level2name = N'updated_on';

