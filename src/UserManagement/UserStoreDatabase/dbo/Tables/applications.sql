﻿CREATE TABLE [dbo].[applications] (
    [id]          BIGINT             NOT NULL,
    [name]        VARCHAR (50)       NOT NULL,
    [app_key]     VARCHAR (50)       NOT NULL,
    [url]         NVARCHAR (250)     NULL,
    [logout_url]  VARCHAR (250)      NULL,
    [created_on]  DATETIMEOFFSET (7) NOT NULL,
    [created_by]  VARCHAR (50)       NOT NULL,
    [modified_on] DATETIMEOFFSET (7) NOT NULL,
    [modified_by] VARCHAR (50)       NOT NULL,
    CONSTRAINT [PK_applications] PRIMARY KEY CLUSTERED ([id] ASC)
);

