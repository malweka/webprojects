﻿CREATE TABLE [dbo].[user_roles] (
    [user_id] BIGINT NOT NULL,
    [role_id] BIGINT NOT NULL,
    CONSTRAINT [PK_user_roles] PRIMARY KEY CLUSTERED ([user_id] ASC, [role_id] ASC),
    CONSTRAINT [FK_user_roles_roles] FOREIGN KEY ([role_id]) REFERENCES [dbo].[roles] ([id]),
    CONSTRAINT [FK_user_roles_users] FOREIGN KEY ([user_id]) REFERENCES [dbo].[users] ([id])
);

