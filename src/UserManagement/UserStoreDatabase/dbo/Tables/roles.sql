﻿CREATE TABLE [dbo].[roles] (
    [id]          BIGINT         NOT NULL,
    [role_number] INT            NOT NULL,
    [role_name]   VARCHAR (50)   NOT NULL,
    [description] NVARCHAR (500) NULL,
    CONSTRAINT [PK_roles] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [IX_roles_name] UNIQUE NONCLUSTERED ([role_name] ASC),
    CONSTRAINT [IX_roles_numb] UNIQUE NONCLUSTERED ([role_number] ASC)
);

