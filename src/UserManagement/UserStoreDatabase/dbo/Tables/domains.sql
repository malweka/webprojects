﻿CREATE TABLE [dbo].[domains] (
    [id]          BIGINT             NOT NULL,
    [domain_name] VARCHAR (128)      NOT NULL,
    [created_on]  DATETIMEOFFSET (7) CONSTRAINT [DF_domains_created_on] DEFAULT (getutcdate()) NOT NULL,
    [created_by]  VARCHAR (128)      CONSTRAINT [DF_domains_created_by] DEFAULT ('System') NOT NULL,
    [modified_on] DATETIMEOFFSET (7) CONSTRAINT [DF_domains_modified_on] DEFAULT (getutcdate()) NOT NULL,
    [modified_by] VARCHAR (128)      CONSTRAINT [DF_domains_modified_by] DEFAULT ('System') NOT NULL,
    [active]      BIT                CONSTRAINT [DF_domains_active] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [domains_pkey] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [IX_domains] UNIQUE NONCLUSTERED ([domain_name] ASC)
);



