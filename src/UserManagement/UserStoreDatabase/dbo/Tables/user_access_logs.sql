﻿CREATE TABLE [dbo].[user_access_logs] (
    [id]            BIGINT             NOT NULL,
    [user_name]     VARCHAR (50)       NOT NULL,
    [activity_type] VARCHAR (50)       NOT NULL,
    [created_on]    DATETIMEOFFSET (7) NOT NULL,
    [ip_address]    VARCHAR (50)       NULL,
    CONSTRAINT [PK_user_activity_logs] PRIMARY KEY CLUSTERED ([id] ASC)
);

