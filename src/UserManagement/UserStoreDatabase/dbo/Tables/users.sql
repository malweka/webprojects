﻿CREATE TABLE [dbo].[users] (
    [id]                      BIGINT             NOT NULL,
    [user_name]               VARCHAR (128)      NOT NULL,
    [first_name]              NVARCHAR (50)      NULL,
    [last_name]               NVARCHAR (50)      NULL,
    [password_hash]           VARCHAR (256)      NOT NULL,
    [last_activity_on]        DATETIMEOFFSET (7) NULL,
    [home]                    VARCHAR (255)      NOT NULL,
    [gid]                     INT                NOT NULL,
    [email_address]           VARCHAR (255)      NULL,
    [external_screen_name]    VARCHAR (50)       NULL,
    [external_user_id]        VARCHAR (50)       NULL,
    [external_email]          VARCHAR (255)      NULL,
    [authentication_provider] VARCHAR (50)       NULL,
    [use_gravatar_profile]    BIT                CONSTRAINT [DF_users_use_gravatar_profile] DEFAULT ((1)) NOT NULL,
    [profile_image_url]       NVARCHAR (250)     NULL,
    [created_on]              DATETIMEOFFSET (7) CONSTRAINT [DF_users_created_on] DEFAULT (getutcdate()) NOT NULL,
    [created_by]              VARCHAR (128)      CONSTRAINT [DF_users_created_by] DEFAULT ('System') NOT NULL,
    [modified_on]             DATETIMEOFFSET (7) CONSTRAINT [DF_users_modified_on] DEFAULT (getutcdate()) NOT NULL,
    [modified_by]             VARCHAR (128)      CONSTRAINT [DF_users_modified_by] DEFAULT ('System') NOT NULL,
    [active]                  BIT                CONSTRAINT [DF_users_active] DEFAULT ((1)) NOT NULL,
    [domain_id]               BIGINT             NULL,
    CONSTRAINT [users_pkey] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK_users_domains] FOREIGN KEY ([domain_id]) REFERENCES [dbo].[domains] ([id]),
    CONSTRAINT [IX_users] UNIQUE NONCLUSTERED ([user_name] ASC)
);



