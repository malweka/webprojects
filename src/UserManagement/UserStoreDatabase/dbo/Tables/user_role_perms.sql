﻿CREATE TABLE [dbo].[user_role_perms] (
    [id]          BIGINT             NOT NULL,
    [resource_id] INT                NOT NULL,
    [role_number] INT                NOT NULL,
    [perm_value]  INT                NOT NULL,
    [created_on]  DATETIMEOFFSET (7) CONSTRAINT [DF_user_role_perms_created_on] DEFAULT (getutcdate()) NOT NULL,
    [created_by]  VARCHAR (50)       NULL,
    [modified_on] DATETIMEOFFSET (7) NULL,
    [modified_by] VARCHAR (50)       NULL,
    CONSTRAINT [PK_user_role_perms] PRIMARY KEY CLUSTERED ([id] ASC)
);

