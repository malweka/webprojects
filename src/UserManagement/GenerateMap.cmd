GoliathData -namespace="Goliath.Ums.Data"^
 -workingFolder="%~dp0GoliathUmsCore\Data\Generated"^
 -baseModel="%~dp0baseModel.xml"^
 -exclude="acct*|dtproperties"^
 -assembly="Goliath.Ums"^
 -connectionstring="Data Source=localhost;Initial Catalog=hosting;Integrated Security=True"^
 -defaultKeygen="Cms_Integer_keyGen"^
 -extension="%~dp0extensions.txt"^
 -complexTypeMap="%~dp0ComplexTypeMap.txt"^
 -renameConfig="%~dp0Renames.txt"^
 -statementMap="%~dp0GoliathUmsCore\Data\mappedStatements.xml"
 
 
 copy /Y "%~dp0GoliathUmsCore\Data\Generated\data.map.xml" "%~dp0GoliathUmsHosting\data.map.config"

 pause