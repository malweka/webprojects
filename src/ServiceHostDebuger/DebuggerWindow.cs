﻿using System;
using System.Windows.Forms;

namespace ServiceHost.Debugger
{
    public partial class DebuggerWindow : Form
    {
        ServiceBase service;

        public DebuggerWindow() : this(null) { }

        public DebuggerWindow(ServiceBase service)
        {
            InitializeComponent();
            if (service != null)
                this.service = service;
            else
                this.service = new NullService();
        }

        private void DebuggerWindow_Load(object sender, EventArgs e)
        {
            StopButton.Enabled = false;
            StartButton.Enabled = true;
            this.Text = Text + ": " + service.ServiceName;
        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            service.Start(new string[] { });
            StopButton.Enabled = true;
            StartButton.Enabled = false;
        }

        private void StopButton_Click(object sender, EventArgs e)
        {
            if (service.CanStop)
            {
                service.Stop();
                StopButton.Enabled = false;
                StartButton.Enabled = true;
            }
            
        }
    }
}
