﻿using System;

namespace ServiceHost.Debugger
{
    public abstract class ServiceBase : System.ComponentModel.Component
    {
        public bool AutoLog { get; set; }

        public bool CanHandlePowerEvent { get; set; }

        public bool CanHandleSessionChangeEvent { get; set; }

        public bool CanPauseAndContinue { get; set; }

        public bool CanShutdown { get; set; }

        public bool CanStop { get; set; }
        

        public int ExitCode { get; set; }
        public string ServiceName { get; set; }

        public ServiceBase()
        {
            CanShutdown = true;
            CanStop = true;
            CanPauseAndContinue = true;
        }

        protected abstract void OnStart(string[] args);
        protected abstract void OnStop();

        internal void Start(string[] args)
        {
            OnStart(args);
        }

        public virtual void Stop()
        {
            OnStop();
        }

        public static void Run(ServiceBase service)
        {
            Console.WriteLine("running {0}", service.ServiceName);
            var debugger = new DebuggerWindow(service);
            debugger.ShowDialog();
        }

        public static void Run(ServiceBase[] services)
        {
            foreach (var service in services)
            {
                Run(service);
            }
        }

        public void RequestAdditionalTime(int milliseconds)
        {
        }
    }
}
