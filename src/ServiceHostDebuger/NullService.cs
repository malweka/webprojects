﻿using System;

namespace ServiceHost.Debugger
{
    class NullService : ServiceBase
    {
        public NullService()
        {
            ServiceName = "Null Service";
        }
        protected override void OnStart(string[] args)
        {
            Console.WriteLine("null service has started");
        }

        protected override void OnStop()
        {
            Console.WriteLine("null service stop");
        }
    }
}
